#!/bin/sh
yes "y" |cp -rf ConfigsToCopy/jbossweb-tomcat55.sar/context.xml /opt/ni2/jboss/server/default/deploy/jbossweb-tomcat55.sar/;
yes "y" |cp -rf ConfigsToCopy/jbossweb-tomcat55.sar/server.xml /opt/ni2/jboss/server/default/deploy/jbossweb-tomcat55.sar/;
yes "y" |cp -rf ConfigsToCopy/ni2config.override.prod.en.properties /opt/ni2/jboss/server/default/ni2config.override.properties;
yes "y" |cp -rf ConfigsToCopy/jms/postgres-jdbc2-service.xml /opt/ni2/jboss/server/default/deploy/jms/;
yes "y" |rm -f /opt/ni2/jboss/server/default/deploy/jms/hsqldb-jdbc2-service.xml;
yes "y" |cp -rf ConfigsToCopy/postgresql-ds.prod.xml /opt/ni2/jboss/server/default/deploy/postgresql-ds.xml;
yes "y" |cp ../resourcesOverride/log4j.prod.en.xml /opt/ni2/jboss/server/default/conf/log4j.xml;
yes "y" |cp -rf ConfigsToCopy/licenses /opt/ni2/jboss/server/default/conf/;
yes "y" |cp -rf ConfigsToCopy/ExtendedJDBCAppender.jar /opt/ni2/jboss/server/default/lib/ExtendedJDBCAppender.jar;
