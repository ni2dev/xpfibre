package com.ni2.modules.covage.wsftth;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Arrays;
import java.util.Map;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.TimeZone;
import java.io.FileOutputStream;

import java.io.IOException;
import java.io.FileNotFoundException;
import javax.xml.soap.SOAPException;
import com.ni2.cmdb.core.CMDBException;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.ni2.cmdb.core.ItemService;
import com.ni2.cmdb.common.ManagedEntityValue;
import com.ni2.cmdb.common.ManagedEntityKey;
import com.ni2.cmdb.common.ManagedEntityValueIterator;
import com.ni2.cmdb.core.CMDB;
import com.ni2.cmdb.model.ClassificationInfo;
import com.ni2.config.Ni2BaseConfig;
import com.ni2.cmdb.module.ResourcesService;

import javax.xml.soap.SOAPMessage;
import javax.xml.ws.soap.SOAPFaultException;
import javax.xml.ws.Dispatch;
import javax.xml.datatype.Duration;
import javax.xml.datatype.DatatypeFactory;
import javax.xml.datatype.DatatypeConfigurationException;

import com.ni2.modules.covage.tools.WSLogsService;

import com.ni2.modules.soaptool.ServiceDetail;
import com.ni2.modules.soaptool.DynamicWebServiceClient;


import com.ni2.modules.ni2application.Ni2AppAbstractCMDBService;




public class WSFTTHUpdateService30Impl extends Ni2AppAbstractCMDBService implements WSFTTHUpdateService30 {
	public static final Log log = LogFactory.getLog(WSFTTHUpdateService30.class);
	private static final String MESSAGE_PREFIX = "(WSFTTHUpdateService30Impl) ";
	private String logLine;
	private Boolean debug = true;


	public WSFTTHUpdateService30Impl(CMDB cmdb) throws CMDBException {
		super(cmdb);
	}

	public String getCMDBServiceName() {
		return CMDB_SERVICE_NAME;
	}

	public void notifyOC_Comment(ManagedEntityValue contextMEV, String comment) throws CMDBException{
		String msg = MESSAGE_PREFIX + "notifyOC_Comment(" + contextMEV + ", " + comment + ")";
		if (log.isDebugEnabled() || debug) log.error(msg);
		Map<String, String> args = buildTTNotificationMAP(contextMEV, "INFO", comment);

		// Optional
		Date supplierPlannedActionDateObj = (Date)contextMEV.getAttributeValue("SupplierPlannedActionDate");
		String supplierPlannedActionDate = null;
		if (supplierPlannedActionDateObj != null) supplierPlannedActionDate = getISO8601Date(supplierPlannedActionDateObj);
		String supplierResolutionAction = (String)contextMEV.getAttributeValue("SupplierResolutionAction");
		String supplierResolutionState = (String)contextMEV.getAttributeValue("SupplierResolutionState");
		args.put("supplierPlannedActionDate",supplierPlannedActionDate);
		args.put("supplierResolutionAction",supplierResolutionAction);
		args.put("supplierResolutionState",supplierResolutionState);
		runAction(contextMEV, args);
	}

	public void notifyOC_DemandeRDV(ManagedEntityValue contextMEV, String comment) throws CMDBException{
		String msg = MESSAGE_PREFIX + "notifyOC_DemandeRDV(" + contextMEV + ", " + comment + ")";
		if (log.isDebugEnabled() || debug) log.error(msg);
		Map<String, String> args = buildTTNotificationResolutionMAP(contextMEV, "DEMANDE RDV", comment);
		runAction(contextMEV, args);
	}

	public void notifyOC_ReponseRDV(ManagedEntityValue contextMEV, String comment) throws CMDBException{
		String msg = MESSAGE_PREFIX + "notifyOC_ReponseRDV(" + contextMEV + ", " + comment + ")";
		if (log.isDebugEnabled() || debug) log.error(msg);
		Map<String, String> args = buildTTNotificationResolutionDateMAP(contextMEV, "REPONSE RDV", comment);
		runAction(contextMEV, args);
	}

	public void notifyOC_StartTroubleTicket(ManagedEntityValue contextMEV) throws CMDBException{
		String msg = MESSAGE_PREFIX + "notifyOC_StartTroubleTicket(" + contextMEV  + ")";
		if (log.isDebugEnabled() || debug) log.error(msg);
		Map<String, String> args = buildTTStartMAP(contextMEV);
		runAction(contextMEV, args);
	}

	public void notifyOC_FreezeTroubleTicket(ManagedEntityValue contextMEV) throws CMDBException{
		String msg = MESSAGE_PREFIX + "notifyOC_FreezeTroubleTicket(" + contextMEV  + ")";
		if (log.isDebugEnabled() || debug) log.error(msg);
		Map<String, String> args = buildTTFreezeMAP(contextMEV);
		runAction(contextMEV, args);
	}

	public void notifyOC_UnFreezeTroubleTicket(ManagedEntityValue contextMEV) throws CMDBException{
		String msg = MESSAGE_PREFIX + "notifyOC_UnFreezeTroubleTicket(" + contextMEV  + ")";
		if (log.isDebugEnabled() || debug) log.error(msg);
		Map<String, String> args = buildTTUnFreezeMAP(contextMEV);
		runAction(contextMEV, args);
	}

	public void notifyOC_ClearTroubleTicket(ManagedEntityValue contextMEV) throws CMDBException{
		String msg = MESSAGE_PREFIX + "notifyOC_ClearTroubleTicket(" + contextMEV  + ")";
		if (log.isDebugEnabled() || debug) log.error(msg);
		Map<String, String> args = buildTTClearMAP(contextMEV);
		runAction(contextMEV, args);
	}

	public void notifyOC_UnClearTroubleTicket(ManagedEntityValue contextMEV) throws CMDBException{
		String msg = MESSAGE_PREFIX + "notifyOC_UnClearTroubleTicket(" + contextMEV  + ")";
		if (log.isDebugEnabled() || debug) log.error(msg);
		Map<String, String> args = buildTTUnClearMAP(contextMEV);
		runAction(contextMEV, args);
	}

	public void notifyOC_WaitingTroubleTicket(ManagedEntityValue contextMEV) throws CMDBException{
		String msg = MESSAGE_PREFIX + "notifyOC_WaitingTroubleTicket(" + contextMEV  + ")";
		if (log.isDebugEnabled() || debug) log.error(msg);
		Map<String, String> args = buildTTWaitingMAP(contextMEV);
		runAction(contextMEV, args);
	}

	public void notifyOC_CloseTroubleTicket(ManagedEntityValue contextMEV) throws CMDBException{
		String msg = MESSAGE_PREFIX + "notifyOC_CloseTroubleTicket(" + contextMEV  + ")";
		if (log.isDebugEnabled() || debug) log.error(msg);
		Map<String, String> args = buildTTCloseMAP(contextMEV);
		runAction(contextMEV, args);
	}

	public void notifyOC_CancelTroubleTicket(ManagedEntityValue contextMEV) throws CMDBException{
		String msg = MESSAGE_PREFIX + "notifyOC_CancelTroubleTicket(" + contextMEV  + ")";
		if (log.isDebugEnabled() || debug) log.error(msg);
		Map<String, String> args = buildTTCancelMAP(contextMEV);
		runAction(contextMEV, args);
	}


	private Map<String, String> buildTTStartMAP(ManagedEntityValue contextMEV) throws CMDBException{
		String msg = MESSAGE_PREFIX + "buildTTStartMAP(" + contextMEV + ")";
		if (log.isDebugEnabled() || debug) log.error(msg);

		Map<String, String> args = buildBasicTroubleTicketMAP(contextMEV);

		// Optional
		Date supplierPlannedActionDateObj = (Date)contextMEV.getAttributeValue("SupplierPlannedActionDate");
		String supplierPlannedActionDate = null;
		if (supplierPlannedActionDateObj != null) supplierPlannedActionDate = getISO8601Date(supplierPlannedActionDateObj);
		String supplierResolutionAction = (String)contextMEV.getAttributeValue("SupplierResolutionAction");
		String delivryPointPM = (String) contextMEV.getAttributeValue("DelivryPointPM");
		String delivryPointPRDM = (String) contextMEV.getAttributeValue("DelivryPointPRDM");
		String gRTDelay = (String) contextMEV.getAttributeValue("GRTDelay");
		String gRTRecoveryRange = (String) contextMEV.getAttributeValue("GRTRecoveryRange");
		String gRTSignalingRangeDay = (String) contextMEV.getAttributeValue("GRTSignalingRangeDay");
		String gRTSignalingRangeHour = (String) contextMEV.getAttributeValue("GRTSignalingRangeHour");

		// Default to GTR as it is the convention today
		// String slaId = "GTR";
		// args.put("slaId",slaId);
		args.put("delivryPointPM",delivryPointPM);
		args.put("delivryPointPRDM",delivryPointPRDM);
		args.put("GRTDelay",gRTDelay);
		args.put("GRTRecoveryRange",gRTRecoveryRange);
		args.put("GRTSignalingRangeDay",gRTSignalingRangeDay);
		args.put("GRTSignalingRangeHour",gRTSignalingRangeHour);
		args.put("supplierResolutionAction",supplierResolutionAction);
		args.put("supplierPlannedActionDate",supplierPlannedActionDate);
		return args;
	}

	private Map<String, String> buildTTCloseMAP(ManagedEntityValue contextMEV) throws CMDBException{
		String msg = MESSAGE_PREFIX + "buildTTCloseMAP(" + contextMEV + ")";
		if (log.isDebugEnabled() || debug) log.error(msg);

		Map<String, String> args = buildBasicTroubleTicketMAP(contextMEV);

		String closureDuration = null;
		Date creationDate = (Date)contextMEV.getAttributeValue("CreationDate");
		String supplierResolutionState = (String) contextMEV.getAttributeValue("SupplierResolutionState");
		if(creationDate != null){
			long creationDate_ms = creationDate.getTime();
			Date today = new Date();
			long today_ms = today.getTime();
			long diff_ms = today_ms - creationDate_ms;
			closureDuration = getISO8601DurationOfMinutes(diff_ms);
		}
		Date serviceRestoredTimeObj = new Date();
		contextMEV.setAttributeValue("ResolutionDate",serviceRestoredTimeObj);
		this.is.updateItem(contextMEV);
		String serviceRestoredTime = null;
		if (serviceRestoredTimeObj != null) serviceRestoredTime = getISO8601Date(serviceRestoredTimeObj);

		if (creationDate == null || closureDuration == null){
			String errorMessage = MESSAGE_PREFIX + "buildTTCloseMAP: creationDate: " + creationDate + " and closureDuration: " + closureDuration + " are mandatory params. They are not all present";
			log.error(errorMessage);
			throw new CMDBException(errorMessage);
		}else{
			args.put("closureDuration",closureDuration);
			args.put("serviceRestoredTime",serviceRestoredTime);
		}
		return args;
	}

	private Map<String, String> buildTTClearMAP(ManagedEntityValue contextMEV) throws CMDBException{
		String msg = MESSAGE_PREFIX + "buildTTClearMAP(" + contextMEV + ")";
		if (log.isDebugEnabled() || debug) log.error(msg);

		Map<String, String> args = buildBasicTroubleTicketMAP(contextMEV);

		// Mandatory
		String defectLocalization = (String) contextMEV.getAttributeValue("DefectLocalization");
		String defectResponsibility = (String) contextMEV.getAttributeValue("DefectResponsibility");
		String troubleTicketClosureCode = (String) contextMEV.getAttributeValue("TroubleTicketClosure_30");
		String troubleTicketClosureLabel = getAttributeComboLabel(contextMEV, "TroubleTicketClosure_30");
		Date interactionDateCompleteObj = (Date)contextMEV.getAttributeValue("InteractionDateComplete");
		String interactionDateComplete = null;
		if (interactionDateCompleteObj != null) interactionDateComplete = getISO8601Date(interactionDateCompleteObj);

		// optional
		String troubleTicketClosureComment = (String) contextMEV.getAttributeValue("TroubleTicketClosureComment");

		if (defectLocalization == null || defectResponsibility == null  || troubleTicketClosureCode == null  || troubleTicketClosureLabel == null || interactionDateComplete == null){
			String errorMessage = MESSAGE_PREFIX + "buildTTClearMAP: defectLocalization: " + defectLocalization + ", defectResponsibility: " + defectResponsibility + ", troubleTicketClosureCode: " + troubleTicketClosureCode + ", troubleTicketClosureLabel: " + troubleTicketClosureLabel+ " and interactionDateComplete: " + interactionDateComplete + " are mandatory params. They are not all present";
			log.error(errorMessage);
			throw new CMDBException(errorMessage);
		}else{
			args.put("defectLocalization",defectLocalization);
			args.put("defectResponsibility",defectResponsibility);
			args.put("troubleTicketClosureCode",troubleTicketClosureCode);
			args.put("troubleTicketClosureLabel",troubleTicketClosureLabel);
			args.put("troubleTicketClosureComment",troubleTicketClosureComment);
			args.put("interactionDateComplete",interactionDateComplete);
		}
		return args;
	}

	private Map<String, String> buildTTUnClearMAP(ManagedEntityValue contextMEV) throws CMDBException{
		String msg = MESSAGE_PREFIX + "buildTTUnClearMAP(" + contextMEV + ")";
		if (log.isDebugEnabled() || debug) log.error(msg);

		Map<String, String> args = buildBasicTroubleTicketMAP(contextMEV);
		// conditional optional for supplierPlannedActionDate
		Date supplierPlannedActionDateObj = (Date)contextMEV.getAttributeValue("SupplierPlannedActionDate");
		String supplierPlannedActionDate = null;
		if (supplierPlannedActionDateObj != null) supplierPlannedActionDate = getISO8601Date(supplierPlannedActionDateObj);
		args.put("supplierPlannedActionDate",supplierPlannedActionDate);
		return args;
	}

	private Map<String, String> buildTTFreezeMAP(ManagedEntityValue contextMEV) throws CMDBException{
		String msg = MESSAGE_PREFIX + "buildTTFreezeMAP(" + contextMEV + ")";
		if (log.isDebugEnabled() || debug) log.error(msg);

		Map<String, String> args = buildBasicTroubleTicketMAP(contextMEV);
		return args;
	}

	private Map<String, String> buildTTUnFreezeMAP(ManagedEntityValue contextMEV) throws CMDBException{
		String msg = MESSAGE_PREFIX + "buildTTUnFreezeMAP(" + contextMEV + ")";
		if (log.isDebugEnabled() || debug) log.error(msg);

		Map<String, String> args = buildBasicTroubleTicketMAP(contextMEV);
		String supplierResolutionAction = (String) contextMEV.getAttributeValue("SupplierResolutionAction");
		// conditional optional for supplierPlannedActionDate
		Date supplierPlannedActionDateObj = (Date)contextMEV.getAttributeValue("SupplierPlannedActionDate");
		String supplierPlannedActionDate = null;
		if (supplierPlannedActionDateObj != null) supplierPlannedActionDate = getISO8601Date(supplierPlannedActionDateObj);
		args.put("supplierPlannedActionDate",supplierPlannedActionDate);
		args.put("supplierResolutionAction",supplierResolutionAction);
		return args;
	}

	private Map<String, String> buildTTWaitingMAP(ManagedEntityValue contextMEV) throws CMDBException{
		String msg = MESSAGE_PREFIX + "buildTTWaitingMAP(" + contextMEV + ")";
		if (log.isDebugEnabled() || debug) log.error(msg);

		Map<String, String> args = buildBasicTroubleTicketMAP(contextMEV);
		return args;
	}

	private Map<String, String> buildTTCancelMAP(ManagedEntityValue contextMEV) throws CMDBException{
		String msg = MESSAGE_PREFIX + "buildTTCancelMAP(" + contextMEV + ")";
		if (log.isDebugEnabled() || debug) log.error(msg);

		Map<String, String> args = buildBasicTroubleTicketMAP(contextMEV);
		// mandatory
		String defectResponsibility = (String) contextMEV.getAttributeValue("DefectResponsibility");
		Date serviceRestoredTimeObj = (Date)contextMEV.getAttributeValue("ResolutionDate");
		String serviceRestoredTime = null;
		if (serviceRestoredTimeObj != null) serviceRestoredTime = getISO8601Date(serviceRestoredTimeObj);
		String troubleTicketClosureCode = (String) contextMEV.getAttributeValue("TroubleTicketClosure_30");
		String troubleTicketClosureLabel = getAttributeComboLabel(contextMEV, "TroubleTicketClosure_30");
		// optional
		String troubleTicketClosureComment = (String) contextMEV.getAttributeValue("TroubleTicketClosureComment");

		if (defectResponsibility == null || serviceRestoredTime == null || troubleTicketClosureCode == null || troubleTicketClosureLabel == null){
			String errorMessage = MESSAGE_PREFIX + "buildTTCancelMAP: defectResponsibility: " + defectResponsibility + " ,serviceRestoredTime: " + serviceRestoredTime + " ,troubleTicketClosureCode: " + troubleTicketClosureCode + " ,troubleTicketClosureLabel: " + troubleTicketClosureLabel + " are mandatory params. They are not all present";
			log.error(errorMessage);
			throw new CMDBException(errorMessage);
		}else{
			args.put("defectResponsibility",defectResponsibility);
			args.put("serviceRestoredTime",serviceRestoredTime);
			args.put("troubleTicketClosureCode",troubleTicketClosureCode);
			args.put("troubleTicketClosureLabel",troubleTicketClosureLabel);
			args.put("troubleTicketClosureComment",troubleTicketClosureComment);
		}
		return args;
	}

	private Map<String, String> buildTTNotificationResolutionDateMAP(ManagedEntityValue contextMEV, String notificationType, String comment) throws CMDBException{
		String msg = MESSAGE_PREFIX + "buildTTNotificationResolutionDateMAP(" + contextMEV + ", " + notificationType + ", " + comment + ")";
		if (log.isDebugEnabled() || debug) log.error(msg);

		Map<String, String> args = buildTTNotificationResolutionMAP(contextMEV, notificationType, comment);

		// Mandatory
		Date supplierPlannedActionDateObj = (Date)contextMEV.getAttributeValue("SupplierPlannedActionDate");
		String supplierPlannedActionDate = null;
		if (supplierPlannedActionDateObj != null) supplierPlannedActionDate = getISO8601Date(supplierPlannedActionDateObj);

		if (supplierPlannedActionDate == null){
			String errorMessage = MESSAGE_PREFIX + "buildTTNotificationResolutionDateMAP: supplierPlannedActionDate: " + supplierPlannedActionDate + " is a mandatory params. It is not present";
			log.error(errorMessage);
			throw new CMDBException(errorMessage);
		}else{
			args.put("supplierPlannedActionDate",supplierPlannedActionDate);
		}
		return args;
	}

	private Map<String, String> buildTTNotificationResolutionMAP(ManagedEntityValue contextMEV, String notificationType, String comment) throws CMDBException{
		String msg = MESSAGE_PREFIX + "buildTTNotificationResolutionMAP(" + contextMEV + ", " + notificationType + ", " + comment + ")";
		if (log.isDebugEnabled() || debug) log.error(msg);

		Map<String, String> args = buildTTNotificationMAP(contextMEV, notificationType, comment);

		// Mandatory
		String supplierResolutionAction = (String)contextMEV.getAttributeValue("SupplierResolutionAction");
		String supplierResolutionState = (String)contextMEV.getAttributeValue("SupplierResolutionState");
		if (supplierResolutionAction == null || supplierResolutionState == null){
			String errorMessage = MESSAGE_PREFIX + "buildTTNotificationResolutionMAP: supplierResolutionAction: " + supplierResolutionAction + " and supplierResolutionState: " + supplierResolutionState + " are mandatory params. They are not all present";
			log.error(errorMessage);
			throw new CMDBException(errorMessage);
		}else{
			args.put("supplierResolutionAction",supplierResolutionAction);
			args.put("supplierResolutionState",supplierResolutionState);
		}
		return args;
	}

	private Map<String, String> buildTTNotificationMAP(ManagedEntityValue contextMEV, String notificationType, String comment) throws CMDBException{
		String msg = MESSAGE_PREFIX + "buildTTNotificationMAP(" + contextMEV + ", " + notificationType + ", " + comment + ")";
		if (log.isDebugEnabled() || debug) log.error(msg);

		Map<String, String> args = buildBasicTroubleTicketMAP(contextMEV);

		// Optional
		String supplierName = (String) contextMEV.getAttributeValue("SupplierName");
		String supplierUnit = (String) contextMEV.getAttributeValue("SupplierUnit");
		String supplierPhoneNumber = (String) contextMEV.getAttributeValue("SupplierPhoneNumber");
		args.put("supplierName",supplierName);
		args.put("supplierUnit",supplierUnit);
		args.put("supplierPhoneNumber",supplierPhoneNumber);

		// Mandatory
		args.put("messageType", notificationType);
		if (comment == null){
			String errorMessage = MESSAGE_PREFIX + "buildTTNotificationMAP: comment: " + comment + " is a mandatory params. It is not present";
			log.error(errorMessage);
			throw new CMDBException(errorMessage);
		}else{
			args.put("messageContent",comment);
		}
		return args;
	}

	private Map<String, String> buildBasicTroubleTicketMAP(ManagedEntityValue contextMEV) throws CMDBException{
		String msg = MESSAGE_PREFIX + "buildBasicTroubleTicketMAP(" + contextMEV + ")";
		if (log.isDebugEnabled() || debug) log.error(msg);

		Map<String, String> args = new LinkedHashMap<String, String>();

		// Mandatory
		String troubleTicketKey = (String)contextMEV.getAttributeValue("Identifier");
		Integer lastUpdateVersionNumberInt = (Integer)contextMEV.getAttributeValue("LastUpdateVersionNumber");
		String lastUpdateVersionNumber = null;
		if (lastUpdateVersionNumberInt != null) lastUpdateVersionNumber = Integer.toString(lastUpdateVersionNumberInt);
		String interactionDate = getISO8601Date(new Date());
		String description = (String)contextMEV.getAttributeValue("ActionDescription_30");
		String troubleTicketState = (String)contextMEV.getAttributeValue("TroubleTicketState");
		String serviceProviderID = (String)contextMEV.getAttributeValue("ServiceProviderID");
		String supplierID = (String)contextMEV.getAttributeValue("SupplierID");
		String serviceProviderTroubleTicketKey = (String)contextMEV.getAttributeValue("ServiceProviderTroubleTicketKey");
		String supplierName = (String) contextMEV.getAttributeValue("SupplierName");
		String supplierPhoneNumber = (String) contextMEV.getAttributeValue("SupplierPhoneNumber");
		String supplierUnit = (String) contextMEV.getAttributeValue("SupplierUnit");
		String reprovisionningId = (String) contextMEV.getAttributeValue("ReprovisionningId");
		String supplierResolutionState = (String)contextMEV.getAttributeValue("SupplierResolutionState");
		String messageContent = (String) contextMEV.getAttributeValue("MessageContent");
		String messageType = (String) contextMEV.getAttributeValue("MessageType");

		if (troubleTicketKey == null || lastUpdateVersionNumber == null || interactionDate == null || description == null || troubleTicketState == null || serviceProviderID == null || supplierID == null || serviceProviderTroubleTicketKey == null){
			String errorMessage = MESSAGE_PREFIX + "buildBasicTroubleTicketMAP: troubleTicketKey: " + troubleTicketKey + ", lastUpdateVersionNumber: " + lastUpdateVersionNumber + ", interactionDate: " + interactionDate + ", description: " + description + ", troubleTicketState: " + troubleTicketState + ", serviceProviderID: " + serviceProviderID + ", supplierID: " + supplierID + " and serviceProviderTroubleTicketKey: " + serviceProviderTroubleTicketKey + " are mandatory params. They are not all present";
			log.error(errorMessage);
			throw new CMDBException(errorMessage);
		}else{
			args.put("lastUpdateVersionNumber",lastUpdateVersionNumber);
			args.put("interactionDate",interactionDate);
			args.put("description",description);
			args.put("troubleTicketKey",troubleTicketKey);
			args.put("troubleTicketState",troubleTicketState);
			args.put("serviceProviderID",serviceProviderID);
			args.put("supplierID",supplierID);
			args.put("serviceProviderTroubleTicketKey",serviceProviderTroubleTicketKey);
			args.put("supplierName",supplierName);
			args.put("supplierUnit",supplierUnit);
			args.put("supplierPhoneNumber",supplierPhoneNumber);
			args.put("messageContent",messageContent);
			args.put("messageType",messageType);
			args.put("reprovisionningId",reprovisionningId);
			args.put("supplierResolutionState",supplierResolutionState);
			contextMEV.setAttributeValue("InteractionDate",new Date());
			this.is.updateItem(contextMEV);
		}
		return args;
	}

	private String getSPName(String codeSiren){
		String msg = MESSAGE_PREFIX + "getSPName(" + codeSiren + ")";
		if (log.isDebugEnabled() || debug) log.debug(msg);
		if(codeSiren != null){
			switch(codeSiren) {
				case "397480930":
					return "BOUY";
				case "488095803":
					return "FREE";
				case "852619352":
					return "IFTR";
				case "380129866":
					return "ORANGE";
				case "343059564":
					return "SFR";
				default:
					return "WSFTTH";
			}
		}
		return null;
	}

	private void runAction(ManagedEntityValue ticketMEV, Map<String, String> args) throws CMDBException{
		if (log.isDebugEnabled() || debug) log.error(MESSAGE_PREFIX + "runAction(" + args + ")");

		OIOCActionsStoreService as = (OIOCActionsStoreService) getCmdb().getCMDBService("OIOCActionsStoreService");
		String troubleTicketCommercialId = (String) ticketMEV.getAttributeValue("CommercialId");
		ticketMEV.setAttributeValue("SAV30SystemStatus", "Pending");
		this.is.updateItem(ticketMEV);
		as.storeOIOCAction(getSPName(args.get("serviceProviderID")), args.get("serviceProviderID"), args.get("troubleTicketKey"), args.get("troubleTicketState"), troubleTicketCommercialId, args.get("description"), args, getCmdb().getLoggedUserName());
		if (log.isDebugEnabled() || debug) log.error("Done runAction\n\n\n");
	}

	private String getISO8601Date(Date date){
		// I don't think it is necessary to bring back to UTC
		// TimeZone tz = TimeZone.getTimeZone("UTC");
		DateFormat df = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSXXX"); // Quoted "Z" to indicate UTC, no timezone offset
		// df.setTimeZone(tz);
		String nowAsISO = df.format(date);
		return nowAsISO;
	}

	private String getISO8601DurationOfMinutes(long durationInMilliSeconds){
		if (log.isDebugEnabled() || debug) log.error(MESSAGE_PREFIX + "getISO8601DurationOfMinutes(" + durationInMilliSeconds + ")");
		try{
			Duration durationObj = DatatypeFactory.newInstance().newDuration(durationInMilliSeconds);
        	System.out.println("durationObj: " + durationObj);
        	return durationObj.toString();
        }catch(DatatypeConfigurationException e){
        	log.error(e, e);
        	return null;
        }
	}

	private String getAttributeComboLabel(ManagedEntityValue contextMEV, String attributeName) throws CMDBException{
		if (log.isDebugEnabled() || debug) log.error(MESSAGE_PREFIX + "getAttributeComboLabel(" + contextMEV + ", " + attributeName + ")");
		String attributeValue = (String) contextMEV.getAttributeValue(attributeName);
		if (log.isDebugEnabled() || debug) log.error(MESSAGE_PREFIX + "attributeValue: " + attributeValue);
		ClassificationInfo ci = this.is.getClassification(contextMEV).toInstanceClassification();
		if (log.isDebugEnabled() || debug) log.error(MESSAGE_PREFIX + "ci: " + ci.toSyntax());
		Map<String, String> acceptedComboValues = this.rs.getComboValues(ci, attributeName);
		if (log.isDebugEnabled() || debug) log.error(MESSAGE_PREFIX + "acceptedComboValues: " + acceptedComboValues);
		String attributeComboLabel = acceptedComboValues.get(attributeValue);
		if (log.isDebugEnabled() || debug) log.error(MESSAGE_PREFIX + "attributeComboLabel: " + attributeComboLabel);
		return attributeComboLabel;
	}
}
