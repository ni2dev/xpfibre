package com.ni2.modules.covage.auto;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Arrays;
import java.util.Map;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Collection;
import java.util.List;
import java.util.ArrayList;
import java.util.TimeZone;
import java.io.FileWriter;

import java.io.IOException;
import java.io.FileNotFoundException;
import com.ni2.cmdb.core.CMDBException;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.ni2.cmdb.core.ItemService;
import com.ni2.cmdb.workflow.WorkflowService;
import com.ni2.cmdb.common.ManagedEntityValue;
import com.ni2.cmdb.common.ManagedEntityKey;
import com.ni2.cmdb.common.ManagedEntityValueIterator;
import com.ni2.cmdb.core.CMDB;
import com.ni2.config.Ni2BaseConfig;

import com.ni2.cmdb.documentation.CommentData;
import com.ni2.cmdb.documentation.DocumentationService;

import com.ni2.modules.ni2application.event.EventToolService;
import com.ni2.modules.ni2foundationimportexport.Ni2FounAbstractCMDBService;



public class GestionDeRelanceServiceImpl extends Ni2FounAbstractCMDBService implements GestionDeRelanceService {
	public static final Log log = LogFactory.getLog(GestionDeRelanceService.class);
	private static final String MESSAGE_PREFIX = "(GestionDeRelanceServiceImpl) ";
	private long relanceTicketDuree = 2880;
	private String relanceTicketBusinessHours = "00:00-24:00";
	private String relanceTicketBusinessDays = "mon-fri";
	private long relanceTacheDuree = 2880;
	private String relanceTacheBusinessHours = "00:00-24:00";
	private String relanceTacheBusinessDays = "mon-fri";
	private Boolean debug = false;
	private EventToolService eventTool;
	private DocumentationService ds;



	public GestionDeRelanceServiceImpl(CMDB cmdb) throws CMDBException {
		super(cmdb);
		this.eventTool = (EventToolService)getCmdb().getCMDBService(EventToolService.CMDB_SERVICE_NAME);
		this.ds = getCmdb().getDocumentationService();
		if(rs.getProperty("XPFibre", "relance.ticket.duree") != null){this.relanceTicketDuree = Long.parseLong(rs.getProperty("XPFibre", "relance.ticket.duree"));}
		if(rs.getProperty("XPFibre", "relance.ticket.businessHours") != null){this.relanceTicketBusinessHours = rs.getProperty("XPFibre", "relance.ticket.businessHours");}
		if(rs.getProperty("XPFibre", "relance.ticket.businessDays") != null){this.relanceTicketBusinessDays = rs.getProperty("XPFibre", "relance.ticket.businessDays");}
		if(rs.getProperty("XPFibre", "relance.tache.duree") != null){this.relanceTacheDuree = Long.parseLong(rs.getProperty("XPFibre", "relance.tache.duree"));}
		if(rs.getProperty("XPFibre", "relance.tache.businessHours") != null){this.relanceTacheBusinessHours = rs.getProperty("XPFibre", "relance.tache.businessHours");}
		if(rs.getProperty("XPFibre", "relance.tache.businessDays") != null){this.relanceTacheBusinessDays = rs.getProperty("XPFibre", "relance.tache.businessDays");}
	}

	public String getCMDBServiceName() {
		return CMDB_SERVICE_NAME;
	}

	public void relancerIncident() throws CMDBException {
		log.info("START - relancerIncident()");
		Date now = new Date();
		Date lastModifDate = eventTool.catculateEndDate(now, relanceTicketDuree * -1, relanceTicketBusinessHours, relanceTicketBusinessDays);
		log.info("	Looking for ticket that haven't been modified since: "+lastModifDate.toString());
		String nqlQuery = "get Event(\"Event/Support/Incident/Service Incident\") where DateDeRelance == null and LastModificationDate <= :lastModifDate and not referenced as ItemKey of (get ResponsibleForAssociation where Responsibility == \"Handler\") and not Status in (\"Idle\", \"Closed\", \"Resolved\", \"Canceled\", \"Completed\", \"Failed\");";
		log.info("	Looking for ticket for: "+nqlQuery);
		Map<String, Object> params = new HashMap<>();
		params.put(":lastModifDate", lastModifDate);
		List<ManagedEntityValue> nouveauTicketRelanceList = query(nqlQuery, params);
		log.info("		Found: "+nouveauTicketRelanceList.size());
		String messageRelanceTemplate = rs.getProperty("XPFibre","relance.ticket.message");
		String messageRelance = messageRelanceTemplate;
		for(ManagedEntityValue ticket : nouveauTicketRelanceList){
			ticket.setAttributeValue("DateDeRelance",now);
			is.updateItem(ticket);
			if(messageRelanceTemplate != null){
				messageRelance = messageRelanceTemplate.replace("xxx",(String)ticket.getAttributeValue("Name"));
				log.info("			Adding comment: "+messageRelance);
				CommentData commentData = ds.makeCommentData("Generic Comment", "Relance de ticket", messageRelance);
				commentData.setInitialAttributeValue("Visibility","Private");
				ds.addDocumentation(ticket.getManagedEntityKey(),commentData);
			}

		}
		log.info("END - relancerIncident()");
	}

	public void relancerTache() throws CMDBException {
		log.info("START - relancerTache()");
		Date now = new Date();
		Date lastModifDate = eventTool.catculateEndDate(now, relanceTacheDuree * -1, relanceTacheBusinessHours, relanceTacheBusinessDays);
		log.info("	Looking for ticket that haven't been modified since: "+lastModifDate.toString());
		String nqlQuery = "get Event(\"Event/Activity/Task\") where DateDeRelance == null and CreationDate <= :lastModifDate and Status in (\"Open\");";
		log.info("	Looking for ticket for: "+nqlQuery);
		Map<String, Object> params = new HashMap<>();
		params.put(":lastModifDate", lastModifDate);
		List<ManagedEntityValue> nouveauTicketRelanceList = query(nqlQuery, params);
		log.info("		Found: "+nouveauTicketRelanceList.size());
		String messageRelanceTemplate = rs.getProperty("XPFibre","relance.tache.message");
		String messageRelance = messageRelanceTemplate;
		for(ManagedEntityValue ticket : nouveauTicketRelanceList){
			ticket.setAttributeValue("DateDeRelance",now);
			is.updateItem(ticket);
			if(messageRelanceTemplate != null){
				messageRelance = messageRelanceTemplate.replace("xxx",(String)ticket.getAttributeValue("Name"));
				log.info("			Adding comment: "+messageRelance);
				CommentData commentData = ds.makeCommentData("Generic Comment", "Relance de ticket", messageRelance);
				commentData.setInitialAttributeValue("Visibility","Private");
				ds.addDocumentation(ticket.getManagedEntityKey(),commentData);
			}
		}
		log.info("END - relancerTache()");
	}

}
