package com.ni2.modules.covage.wsftth;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Arrays;
import java.util.Map;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.TimeZone;
import java.io.FileOutputStream;

import java.io.IOException;
import java.io.FileNotFoundException;
import javax.xml.soap.SOAPException;
import com.ni2.cmdb.core.CMDBException;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.ni2.cmdb.core.ItemService;
import com.ni2.cmdb.common.ManagedEntityValue;
import com.ni2.cmdb.common.ManagedEntityKey;
import com.ni2.cmdb.common.ManagedEntityValueIterator;
import com.ni2.cmdb.core.CMDB;
import com.ni2.cmdb.model.ClassificationInfo;
import com.ni2.config.Ni2BaseConfig;
import com.ni2.cmdb.module.ResourcesService;

import javax.xml.soap.SOAPMessage;
import javax.xml.ws.soap.SOAPFaultException;
import javax.xml.ws.Dispatch;
import javax.xml.datatype.Duration;
import javax.xml.datatype.DatatypeFactory;
import javax.xml.datatype.DatatypeConfigurationException;

import com.ni2.modules.covage.wsftth.BOUYServiceDetail;
import com.ni2.modules.covage.wsftth.FREEServiceDetail;
import com.ni2.modules.covage.wsftth.IFTRServiceDetail;
import com.ni2.modules.covage.wsftth.ORANGEServiceDetail;
import com.ni2.modules.covage.wsftth.SFRServiceDetail;
import com.ni2.modules.covage.tools.WSLogsService;

import com.ni2.modules.soaptool.ServiceDetail;
import com.ni2.modules.soaptool.DynamicWebServiceClient;


import com.ni2.modules.ni2application.Ni2AppAbstractCMDBService;




public class WSFTTHUpdateServiceImpl extends Ni2AppAbstractCMDBService implements WSFTTHUpdateService {
	public static final Log log = LogFactory.getLog(WSFTTHUpdateService.class);
	private static final String MESSAGE_PREFIX = "(WSFTTHUpdateServiceImpl) ";
	private String logLine;
	private Boolean debug = false;


	public WSFTTHUpdateServiceImpl(CMDB cmdb) throws CMDBException {
		super(cmdb);
	}

	public String getCMDBServiceName() {
		return CMDB_SERVICE_NAME;
	}

	public void notifyOC_Comment(ManagedEntityValue contextMEV, String comment) throws CMDBException{
		String msg = MESSAGE_PREFIX + "notifyOC_Comment(" + contextMEV + ", " + comment + ")";
		if (log.isDebugEnabled() || debug) log.debug(msg);
		Map<String, Object> args = buildTTNotificationMAP(contextMEV, "INFO", comment);

		// Optional
		Date supplierPlannedActionDateObj = (Date)contextMEV.getAttributeValue("SupplierPlannedActionDate");
		String supplierPlannedActionDate = null;
		if (supplierPlannedActionDateObj != null) supplierPlannedActionDate = getISO8601Date(supplierPlannedActionDateObj);
		String supplierResolutionAction = (String)contextMEV.getAttributeValue("SupplierResolutionAction");
		String supplierResolutionState = (String)contextMEV.getAttributeValue("SupplierResolutionState");
		args.put("ns4:supplierPlannedActionDate",supplierPlannedActionDate);
		args.put("ns4:supplierResolutionAction",supplierResolutionAction);
		args.put("ns4:supplierResolutionState",supplierResolutionState);
		notifyOC(contextMEV, args);
	}

	public void notifyOC_DemandeRDV(ManagedEntityValue contextMEV, String comment) throws CMDBException{
		String msg = MESSAGE_PREFIX + "notifyOC_DemandeRDV(" + contextMEV + ", " + comment + ")";
		if (log.isDebugEnabled() || debug) log.debug(msg);
		Map<String, Object> args = buildTTNotificationResolutionMAP(contextMEV, "DEMANDE RDV", comment);
		notifyOC(contextMEV, args);
	}

	public void notifyOC_ReponseRDV(ManagedEntityValue contextMEV, String comment) throws CMDBException{
		String msg = MESSAGE_PREFIX + "notifyOC_ReponseRDV(" + contextMEV + ", " + comment + ")";
		if (log.isDebugEnabled() || debug) log.debug(msg);
		Map<String, Object> args = buildTTNotificationResolutionDateMAP(contextMEV, "REPONSE RDV", comment);
		notifyOC(contextMEV, args);
	}

	public void notifyOC_StartTroubleTicket(ManagedEntityValue contextMEV) throws CMDBException{
		String msg = MESSAGE_PREFIX + "notifyOC_StartTroubleTicket(" + contextMEV  + ")";
		if (log.isDebugEnabled() || debug) log.debug(msg);
		Map<String, Object> args = buildTTStartMAP(contextMEV);
		notifyOC(contextMEV, args);
	}

	public void notifyOC_CloseTroubleTicket(ManagedEntityValue contextMEV) throws CMDBException{
		String msg = MESSAGE_PREFIX + "notifyOC_CloseTroubleTicket(" + contextMEV  + ")";
		if (log.isDebugEnabled() || debug) log.debug(msg);
		Map<String, Object> args = buildTTCloseMAP(contextMEV);
		notifyOC(contextMEV, args);
	}

	public void notifyOC_CancelTroubleTicket(ManagedEntityValue contextMEV) throws CMDBException{
		String msg = MESSAGE_PREFIX + "notifyOC_CancelTroubleTicket(" + contextMEV  + ")";
		if (log.isDebugEnabled() || debug) log.debug(msg);
		Map<String, Object> args = buildTTBasicClosureMAP(contextMEV);
		notifyOC(contextMEV, args);
	}

	private void notifyOC(ManagedEntityValue contextMEV, Map<String, Object> args) throws CMDBException{
		String msg = MESSAGE_PREFIX + "notifyOC(" + contextMEV + ", " + args + ")";
		if (log.isDebugEnabled() || debug) log.debug(msg);

		String codeSiren = (String) contextMEV.getAttributeValue("ServiceProviderID");

		ServiceDetail sd = null;
		sd = getServiceDetail(codeSiren);

		DynamicWebServiceClient webServiceClient = new DynamicWebServiceClient();

		makeSOAPCall(sd, webServiceClient, "setTroubleTicketByValueRequest", args);
	}

	private Map<String,Object> buildTTStartMAP(ManagedEntityValue contextMEV) throws CMDBException{
		String msg = MESSAGE_PREFIX + "buildTTStartMAP(" + contextMEV + ")";
		if (log.isDebugEnabled() || debug) log.debug(msg);

		Map<String, Object> args = buildBasicTroubleTicketMAP(contextMEV);

		// Optional
		Date supplierPlannedActionDateObj = (Date)contextMEV.getAttributeValue("SupplierPlannedActionDate");
		String supplierPlannedActionDate = null;
		if (supplierPlannedActionDateObj != null) supplierPlannedActionDate = getISO8601Date(supplierPlannedActionDateObj);
		String supplierResolutionAction = (String)contextMEV.getAttributeValue("SupplierResolutionAction");
		args.put("ns4:supplierPlannedActionDate",supplierPlannedActionDate);
		args.put("ns4:supplierResolutionAction",supplierResolutionAction);

		// Mandatory
		String supplierName = (String) contextMEV.getAttributeValue("SupplierName");
		String supplierUnit = (String) contextMEV.getAttributeValue("SupplierUnit");
		String supplierPhoneNumber = (String) contextMEV.getAttributeValue("SupplierPhoneNumber");
		// Default to GTR as it is the convention today
		String slaId = "GTR";
		String supplierResolutionState = (String) contextMEV.getAttributeValue("SupplierResolutionState");
		if (supplierName == null || supplierUnit == null || supplierPhoneNumber == null || supplierResolutionState == null){
			String errorMessage = MESSAGE_PREFIX + "buildTTStartMAP: supplierName: " + supplierName + ", supplierUnit: " + supplierUnit + ", supplierPhoneNumber: " + supplierPhoneNumber + " and supplierResolutionState: " + supplierResolutionState + " are mandatory params. They are not all present";
			log.error(errorMessage);
			throw new CMDBException(errorMessage);
		}else{
			args.put("ns4:supplierName",supplierName);
			args.put("ns4:supplierUnit",supplierUnit);
			args.put("ns4:supplierPhoneNumber",supplierPhoneNumber);
			args.put("ns4:slaId",slaId);
			args.put("ns4:supplierResolutionState",supplierResolutionState);
		}
		return args;
	}

	private Map<String,Object> buildTTCloseMAP(ManagedEntityValue contextMEV) throws CMDBException{
		String msg = MESSAGE_PREFIX + "buildTTCloseMAP(" + contextMEV + ")";
		if (log.isDebugEnabled() || debug) log.debug(msg);

		Map<String, Object> args = buildTTBasicClosureMAP(contextMEV);

		// Mandatory
		String defectLocalization = (String) contextMEV.getAttributeValue("DefectLocalization");
		String closureDuration = null;
		Date creationDate = (Date)contextMEV.getAttributeValue("CreationDate");
		if(creationDate != null){
			long creationDate_ms = creationDate.getTime();
			Date today = new Date();
			long today_ms = today.getTime();
			long diff_ms = today_ms - creationDate_ms;
			closureDuration = getISO8601DurationOfMinutes(diff_ms);
		}

		if (defectLocalization == null || closureDuration == null){
			String errorMessage = MESSAGE_PREFIX + "buildTTCloseMAP: defectLocalization: " + defectLocalization + " and closureDuration: " + closureDuration + " are mandatory params. They are not all present";
			log.error(errorMessage);
			throw new CMDBException(errorMessage);
		}else{
			args.put("ns4:defectLocalization",defectLocalization);
			args.put("ns4:closureDuration",closureDuration);
		}
		return args;
	}

	private Map<String,Object> buildTTBasicClosureMAP(ManagedEntityValue contextMEV) throws CMDBException{
		String msg = MESSAGE_PREFIX + "buildTTBasicClosureMAP(" + contextMEV + ")";
		if (log.isDebugEnabled() || debug) log.debug(msg);

		Map<String, Object> args = buildBasicTroubleTicketMAP(contextMEV);

		// Optional
		String reprovisionningId = (String)contextMEV.getAttributeValue("ReprovisionningId");
		args.put("ns4:reprovisionningId",reprovisionningId);
		String troubleTicketClosureComment = (String)contextMEV.getAttributeValue("TroubleTicketClosureComment");
		args.put("ns4:troubleTicketClosureComment",troubleTicketClosureComment);

		// Mandatory
		Date serviceRestoredTimeObj = (Date)contextMEV.getAttributeValue("ResolutionDate");
		String serviceRestoredTime = null;
		if (serviceRestoredTimeObj != null) serviceRestoredTime = getISO8601Date(serviceRestoredTimeObj);
		String supplierResolutionState = (String) contextMEV.getAttributeValue("SupplierResolutionState");
		String troubleTicketClosureCode = (String) contextMEV.getAttributeValue("TroubleTicketClosure");
		String troubleTicketClosureLabel = getAttributeComboLabel(contextMEV, "TroubleTicketClosure");
		String defectResponsibility = (String) contextMEV.getAttributeValue("DefectResponsibility");

		if (serviceRestoredTime == null || supplierResolutionState == null || troubleTicketClosureCode == null || troubleTicketClosureLabel == null || defectResponsibility == null){
			String errorMessage = MESSAGE_PREFIX + "buildTTBasicClosureMAP: serviceRestoredTime: " + serviceRestoredTime + ", supplierResolutionState: " + supplierResolutionState + ", troubleTicketClosureCode: " + troubleTicketClosureCode + ", troubleTicketClosureLabel: " + troubleTicketClosureLabel + " and defectResponsibility: " + defectResponsibility + " are mandatory params. They are not all present";
			log.error(errorMessage);
			throw new CMDBException(errorMessage);
		}else{
			args.put("ns3:serviceRestoredTime",serviceRestoredTime);
			args.put("ns4:supplierResolutionState",supplierResolutionState);
			args.put("ns4:troubleTicketClosureCode",troubleTicketClosureCode);
			args.put("ns4:troubleTicketClosureLabel",troubleTicketClosureLabel);
			args.put("ns4:defectResponsibility",defectResponsibility);
		}
		return args;
	}

	private Map<String,Object> buildTTNotificationResolutionDateMAP(ManagedEntityValue contextMEV, String notificationType, String comment) throws CMDBException{
		String msg = MESSAGE_PREFIX + "buildTTNotificationResolutionDateMAP(" + contextMEV + ", " + notificationType + ", " + comment + ")";
		if (log.isDebugEnabled() || debug) log.debug(msg);

		Map<String, Object> args = buildTTNotificationResolutionMAP(contextMEV, notificationType, comment);

		// Mandatory
		Date supplierPlannedActionDateObj = (Date)contextMEV.getAttributeValue("SupplierPlannedActionDate");
		String supplierPlannedActionDate = null;
		if (supplierPlannedActionDateObj != null) supplierPlannedActionDate = getISO8601Date(supplierPlannedActionDateObj);

		if (supplierPlannedActionDate == null){
			String errorMessage = MESSAGE_PREFIX + "buildTTNotificationResolutionDateMAP: supplierPlannedActionDate: " + supplierPlannedActionDate + " is a mandatory params. It is not present";
			log.error(errorMessage);
			throw new CMDBException(errorMessage);
		}else{
			args.put("ns4:supplierPlannedActionDate",supplierPlannedActionDate);
		}
		return args;
	}

	private Map<String,Object> buildTTNotificationResolutionMAP(ManagedEntityValue contextMEV, String notificationType, String comment) throws CMDBException{
		String msg = MESSAGE_PREFIX + "buildTTNotificationResolutionMAP(" + contextMEV + ", " + notificationType + ", " + comment + ")";
		if (log.isDebugEnabled() || debug) log.debug(msg);

		Map<String, Object> args = buildTTNotificationMAP(contextMEV, notificationType, comment);

		// Mandatory
		String supplierResolutionAction = (String)contextMEV.getAttributeValue("SupplierResolutionAction");
		String supplierResolutionState = (String)contextMEV.getAttributeValue("SupplierResolutionState");
		if (supplierResolutionAction == null || supplierResolutionState == null){
			String errorMessage = MESSAGE_PREFIX + "buildTTNotificationResolutionMAP: supplierResolutionAction: " + supplierResolutionAction + " and supplierResolutionState: " + supplierResolutionState + " are mandatory params. They are not all present";
			log.error(errorMessage);
			throw new CMDBException(errorMessage);
		}else{
			args.put("ns4:supplierResolutionAction",supplierResolutionAction);
			args.put("ns4:supplierResolutionState",supplierResolutionState);
		}
		return args;
	}

	private Map<String,Object> buildTTNotificationMAP(ManagedEntityValue contextMEV, String notificationType, String comment) throws CMDBException{
		String msg = MESSAGE_PREFIX + "buildTTNotificationMAP(" + contextMEV + ", " + notificationType + ", " + comment + ")";
		if (log.isDebugEnabled() || debug) log.debug(msg);

		Map<String, Object> args = buildBasicTroubleTicketMAP(contextMEV);

		// Optional
		String supplierName = (String) contextMEV.getAttributeValue("SupplierName");
		String supplierUnit = (String) contextMEV.getAttributeValue("SupplierUnit");
		String supplierPhoneNumber = (String) contextMEV.getAttributeValue("SupplierPhoneNumber");
		args.put("ns4:supplierName",supplierName);
		args.put("ns4:supplierUnit",supplierUnit);
		args.put("ns4:supplierPhoneNumber",supplierPhoneNumber);

		// Mandatory
		args.put("ns4:messageType", notificationType);
		if (comment == null){
			String errorMessage = MESSAGE_PREFIX + "notifyOC_ReponseRDV: comment: " + comment + " is a mandatory params. It is not present";
			log.error(errorMessage);
			throw new CMDBException(errorMessage);
		}else{
			args.put("ns4:messageContent",comment);
		}
		return args;
	}

	private Map<String,Object> buildBasicTroubleTicketMAP(ManagedEntityValue contextMEV) throws CMDBException{
		String msg = MESSAGE_PREFIX + "buildBasicSOAPMAP(" + contextMEV + ")";
		if (log.isDebugEnabled() || debug) log.debug(msg);

		Map<String, Object> args = new LinkedHashMap<String, Object>();

		// Mandatory
		String troubleTicketKey = (String)contextMEV.getAttributeValue("Identifier");
		Integer lastUpdateVersionNumberInt = (Integer)contextMEV.getAttributeValue("LastUpdateVersionNumber");
		String lastUpdateVersionNumber = null;
		if (lastUpdateVersionNumberInt != null) lastUpdateVersionNumber = Integer.toString(lastUpdateVersionNumberInt);
		String interactionDate = getISO8601Date(new Date());
		String description = (String)contextMEV.getAttributeValue("ActionDescription");
		String troubleTicketState = (String)contextMEV.getAttributeValue("TroubleTicketState");
		String serviceProviderID = (String)contextMEV.getAttributeValue("ServiceProviderID");
		String supplierID = (String)contextMEV.getAttributeValue("SupplierID");
		String serviceProviderTroubleTicketKey = (String)contextMEV.getAttributeValue("ServiceProviderTroubleTicketKey");

		if (troubleTicketKey == null || lastUpdateVersionNumber == null || interactionDate == null || description == null || troubleTicketState == null || serviceProviderID == null || supplierID == null || serviceProviderTroubleTicketKey == null){
			String errorMessage = MESSAGE_PREFIX + "buildBasicSOAPMAP: troubleTicketKey: " + troubleTicketKey + ", lastUpdateVersionNumber: " + lastUpdateVersionNumber + ", interactionDate: " + interactionDate + ", description: " + description + ", troubleTicketState: " + troubleTicketState + ", serviceProviderID: " + serviceProviderID + ", supplierID: " + supplierID + " and serviceProviderTroubleTicketKey: " + serviceProviderTroubleTicketKey + " are mandatory params. They are not all present";
			log.error(errorMessage);
			throw new CMDBException(errorMessage);
		}else{
			args.put("ns1:lastUpdateVersionNumber",lastUpdateVersionNumber);
			args.put("ns2:interactionDate",interactionDate);
			args.put("ns2:description",description);
			args.put("ns3:troubleTicketKey",troubleTicketKey);
			args.put("ns3:troubleTicketState",troubleTicketState);
			args.put("ns4:serviceProviderID",serviceProviderID);
			args.put("ns4:supplierID",supplierID);
			args.put("ns4:serviceProviderTroubleTicketKey",serviceProviderTroubleTicketKey);
			args.put("serviceProviderName",getSPName(serviceProviderID));
		}
		return args;
	}

	private ServiceDetail getServiceDetail(String codeSiren) throws CMDBException{
		String msg = MESSAGE_PREFIX + "getServiceDetail(" + codeSiren + ")";
		if (log.isDebugEnabled() || debug) log.debug(msg);
		ServiceDetail sd = null;
		if(codeSiren == null)codeSiren = "123456";
		if(codeSiren != null){
			try {
				switch(codeSiren) {
					case "397480930":
						sd = new BOUYServiceDetail(this.rs);
						break;
					case "488095803":
						sd = new FREEServiceDetail(this.rs);
						break;
					case "852619352":
						sd = new IFTRServiceDetail(this.rs);
						break;
					case "380129866":
						sd = new ORANGEServiceDetail(this.rs);
						break;
					case "343059564":
						sd = new SFRServiceDetail(this.rs);
						break;
					default:
						sd = new WSFTTHServiceDetail(this.rs);
				}
			} catch (Exception e) {
				String errorMessage = MESSAGE_PREFIX + "getServiceDetail: An error occurred while building the Service Details" + e;
				log.error(errorMessage, e);
				throw new CMDBException(errorMessage, e);
			}
		}else{
			String errorMessage = MESSAGE_PREFIX + "getServiceDetail: Returned Default ServiceDetail because no codeSiren was given";
			log.error(errorMessage);
		}
		return sd;
	}

	private String getSPName(String codeSiren){
		String msg = MESSAGE_PREFIX + "getSPName(" + codeSiren + ")";
		if (log.isDebugEnabled() || debug) log.debug(msg);
		if(codeSiren != null){
			switch(codeSiren) {
				case "397480930":
					return "BOUY";
				case "488095803":
					return "FREE";
				case "852619352":
					return "IFTR";
				case "380129866":
					return "ORANGE";
				case "343059564":
					return "SFR";
				default:
					return "WSFTTH";
			}
		}
		return null;
	}

	private void makeSOAPCall(ServiceDetail sd, DynamicWebServiceClient webServiceClient, String operationName, Map<String, Object> args) throws CMDBException{
		if (log.isDebugEnabled() || debug) log.debug(MESSAGE_PREFIX + "makeSOAPCall(sd, webServiceClient, " + operationName + ", " + args + ")");
		if (log.isDebugEnabled() || debug) log.debug(sd.toString());
		if (log.isDebugEnabled() || debug) log.debug(webServiceClient.toString());

		String makeCall = this.rs.getProperty("XPFibre","ws.ftth.makeCall");
		String wsLogProcess = this.rs.getProperty("XPFibre","ws.ftth.wsLogProcess");
		String wsLogDB = this.rs.getProperty("XPFibre","ws.ftth.wsLogs.wsLogDB");
		WSLogsService wls = (WSLogsService) getCmdb().getCMDBService("WSLogsService");
		Boolean success = true;
		String requestStr = "";
		String responseStr = "";
		try {


			Dispatch dispatcher = webServiceClient.buildDispatch(sd);
			SOAPMessage request = webServiceClient.buildSOAPMessage(sd, operationName, args);
			if (log.isDebugEnabled() || debug) log.debug("Request: " + request.toString());
			requestStr = webServiceClient.outputSoapMessage(request, true);
			if (log.isDebugEnabled() || debug) log.debug("Request: " + requestStr);
			if(makeCall != null && makeCall.equals("true")){
				try {
					SOAPMessage response = webServiceClient.invokeOperation(sd, operationName, args);
					if (log.isDebugEnabled() || debug) log.debug("Response: " + response.toString());
					responseStr = webServiceClient.outputSoapMessage(response, true);
				}catch(SOAPFaultException sfex){
					responseStr = sfex.getMessage() + "\n" + sfex.getFault();
					String errorMessage = MESSAGE_PREFIX + "makeSOAPCall: An error occurred while making SOAP Call: " + responseStr;
					log.error(errorMessage, sfex);
					success = false;
				}catch(Exception ex){
					responseStr = ex.getMessage();
					String errorMessage = MESSAGE_PREFIX + "makeSOAPCall: An error occurred while making SOAP Call: " + responseStr;
					log.error(errorMessage, ex);
					success = false;
				}
				if (log.isDebugEnabled() || debug) log.debug("Done invoking Operation");
				if (log.isDebugEnabled() || debug) log.debug("Response: " + responseStr);
			}

			if(wsLogProcess != null && wsLogProcess.equals("true")){
				if (log.isDebugEnabled() || debug) log.debug("Logging call in Ni2 Process");
				wls.wsLogProcess(sd.toString(), requestStr, responseStr, (String)args.get("ns3:troubleTicketKey"), (String)args.get("ns2:description"), success);
			}

			if(wsLogDB != null && wsLogDB.equals("true")){
				if (log.isDebugEnabled() || debug) log.debug("Logging call in wsLog DB");
				wls.wsLogDB((String)args.get("serviceProviderName"), (String)args.get("ns4:serviceProviderID"), "setTroubleTicketByValue", requestStr, responseStr, success, "returnLabel", (String)args.get("ns3:troubleTicketKey"), "message");
			}

		} catch (Exception e) {
			String errorMessage = MESSAGE_PREFIX + "makeSOAPCall: An error occurred while making SOAP Call" + e.getMessage();
			log.error(errorMessage, e);
			if(wsLogProcess != null && wsLogProcess.equals("true")){
				if (log.isDebugEnabled() || debug) log.debug("Logging call in Ni2 Process");
				wls.wsLogProcess(sd.toString(), requestStr, errorMessage, (String)args.get("ns3:troubleTicketKey"), (String)args.get("ns2:description"), false);
			}

			if(wsLogDB != null && wsLogDB.equals("true")){
				if (log.isDebugEnabled() || debug) log.debug("Logging call in wsLog DB");
				wls.wsLogDB((String)args.get("serviceProviderName"), (String)args.get("ns4:serviceProviderID"), "setTroubleTicketByValue", requestStr, errorMessage, false, "returnLabel", (String)args.get("ns3:troubleTicketKey"), "message");
			}
			throw new CMDBException(errorMessage, e);
		}
		if (log.isDebugEnabled() || debug) log.debug("Done testServiceDetail\n\n\n");
	}

	private String getISO8601Date(Date date){
		// I don't think it is necessary to bring back to UTC
		// TimeZone tz = TimeZone.getTimeZone("UTC");
		DateFormat df = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSXXX"); // Quoted "Z" to indicate UTC, no timezone offset
		// df.setTimeZone(tz);
		String nowAsISO = df.format(date);
		return nowAsISO;
	}

	private String getISO8601DurationOfMinutes(long durationInMilliSeconds){
		if (log.isDebugEnabled() || debug) log.debug(MESSAGE_PREFIX + "getISO8601DurationOfMinutes(" + durationInMilliSeconds + ")");
		try{
			Duration durationObj = DatatypeFactory.newInstance().newDuration(durationInMilliSeconds);
        	System.out.println("durationObj: " + durationObj);
        	return durationObj.toString();
        }catch(DatatypeConfigurationException e){
        	log.error(e, e);
        	return null;
        }
	}

	private String getAttributeComboLabel(ManagedEntityValue contextMEV, String attributeName) throws CMDBException{
		String attributeValue = (String) contextMEV.getAttributeValue(attributeName);
		ClassificationInfo ci = this.is.getClassification(contextMEV).toInstanceClassification();
		Map<String, String> acceptedComboValues = this.rs.getComboValues(ci, attributeName);
		String attributeComboLabel = acceptedComboValues.get(attributeValue);
		return attributeComboLabel;
	}
}
