package com.ni2.modules.covage.tools;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.text.DecimalFormat;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.ni2.cmdb.common.ManagedEntityKey;
import com.ni2.cmdb.common.ManagedEntityValue;
import com.ni2.cmdb.core.CMDB;
import com.ni2.cmdb.core.CMDBException;
import com.ni2.cmdb.core.AbstractCMDBService;
import com.ni2.cmdb.core.IdentifiableValue;
import com.ni2.cmdb.core.LocalQueryService;
import com.ni2.cmdb.core.NameableValue;
import com.ni2.cmdb.management.EventValue;
import com.ni2.cmdb.system.ObjectInstanceValue;
import com.ni2.cmdb.system.ObjectTypeValue;
import com.ni2.modules.configurationmanagement.ConfigurationManagementService;
import com.ni2.modules.configurationmanagement.ChangeBean;
import com.ni2.modules.ni2foundationimportexport.genericexport.AbstractNQLReport;
import com.ni2.modules.ni2foundationimportexport.genericexport.tools.ManagedEntityReader;
import com.ni2.modules.ni2foundationimportexport.genericexport.tools.export.CSVExportTools;
import com.ni2.modules.ni2foundationimportexport.genericexport.tools.export.ExportTools;


public class CovageExportToolsImpl extends AbstractCMDBService implements CovageExportTools {
	public static final Log log = LogFactory.getLog(CovageExportToolsImpl.class);

	private final ManagedEntityReader et;

	public CovageExportToolsImpl(CMDB cmdb) throws CMDBException {
		super(cmdb);
		this.et = new ManagedEntityReader(cmdb);
	}

	public String getCMDBServiceName() {
		return CMDB_SERVICE_NAME;
	}

	public String getEventTotalAwaitingInput(ManagedEntityValue event) throws CMDBException{
		String statusList = ",Awaiting Input,";
		Date creationDate = (Date)event.getAttributeValue("CreationDate");
		ConfigurationManagementService cms = (ConfigurationManagementService)getCmdb().getCMDBService("ConfigurationManagementService");
		List<ChangeBean> historyList = cms.getHistory(event);
		Date prevDate = creationDate;
		int awaitInputTimeSec = 0;
		for (ChangeBean historyItem : historyList) {
			String attName = historyItem.getAttributeName();
			if (attName != null && attName.equals("Status")) {
				String oldStatus = historyItem.getOldStatus();
				if (statusList.contains(","+oldStatus+",")) {
					Date curDate = historyItem.getDate();
					Long curDateMS = curDate.getTime();
					Long prevDateMS = prevDate.getTime();
					Long diffMS = curDateMS - prevDateMS;
					Long sec = 0L;
					if (diffMS > 60000) {
						sec = (diffMS - (diffMS % 60000)) / 1000;
					}
					awaitInputTimeSec = awaitInputTimeSec + sec.intValue();
				}
				prevDate = historyItem.getDate();
			}
		}
		Long awaitInputTimeMin = awaitInputTimeSec / 60L;
		Long h = 0L;
		Long m = 0L;
		if (awaitInputTimeMin >= 60) {
			h = (awaitInputTimeMin - (awaitInputTimeMin % 60)) / 60;
			awaitInputTimeMin = awaitInputTimeMin - (h * 60);
		}
		m = awaitInputTimeMin;
		DecimalFormat df = new DecimalFormat( "00" );
		String hStr = df.format(h);
		String mStr = df.format(m);
		String durationDHM = hStr+":"+mStr;
		return durationDHM;
	}

	public String getEventResolutionDate(ManagedEntityValue event, String datePattern) throws CMDBException{
		String returnVal = "";
		Date resolutionDate = null;
		ConfigurationManagementService cms = (ConfigurationManagementService)getCmdb().getCMDBService("ConfigurationManagementService");
		List<ChangeBean> historyList = cms.getHistory(event);
		for (ChangeBean historyItem : historyList) {
			String attName = historyItem.getAttributeName();
			String action = historyItem.getChangeTypeDescription();
			if (attName != null && attName.equals("Status") && action.equals("update") ) {
				String newStatus = historyItem.getNewStatus();
				if (newStatus.equals("Resolved")) {
					resolutionDate = historyItem.getDate();
				}
			}
		}
		SimpleDateFormat df = new SimpleDateFormat(datePattern);
		if(resolutionDate != null)returnVal = df.format(resolutionDate);
		return returnVal;
	}

	public String getEventFormattedTime(ManagedEntityValue event, String attributeName){
		Integer tmp = (Integer)event.getAttributeValue("ResolutionTime");
		if (tmp != null) {
			Long duration = new Long(tmp);
			Long h = 0L;
			Long m = 0L;

			if (duration >= 60) {
			 	h = (duration - (duration % 60)) / 60;
			 	duration = duration - (h * 60);
			}
			m = duration;
			DecimalFormat df = new DecimalFormat( "00" );
			String hStr = df.format(h);
			String mStr = df.format(m);
			String durationDHM = hStr+":"+mStr;
			return durationDHM;
		} else {
			return "";
		}
	}
	public String getEventResolutionTimeDelay(ManagedEntityValue event, String gtr){
		Integer tmp = (Integer)event.getAttributeValue("ResolutionTime");
		if (tmp != null && gtr != null && !gtr.equals("")) {
			Long duration = new Long(tmp);
			Long h = 0L;
			Long m = 0L;

			switch(gtr)
			{
				case "12H":
				case "12h":
					duration -= 12 * 60;
					break;
				case "240H":
					duration -= 240 * 60;
					break;
				case "24H":
				case "24h":
					duration -= 24 * 60;
					break;
				case "4H":
				case "4H - 24H/24 7J/7":
				case "4H - Heure Ouvr":
				case "4H - Heure Ouvr�e":
				case "4HOFF":
				case "4h":
					duration -= 4 * 60;
					break;
				case "8 h":
				case "8H":
				case "8h":
					duration -= 8 * 60;
					break;
				default:
					duration = 0L;
			}

			if (duration >= 60) {
			 	h = (duration - (duration % 60)) / 60;
			 	duration = duration - (h * 60);
			}else if(duration < 0){
				duration = 0L;
			}
			m = duration;
			DecimalFormat df = new DecimalFormat( "00" );
			String hStr = df.format(h);
			String mStr = df.format(m);
			String durationDHM = hStr+":"+mStr;
			return durationDHM;
		} else {
			return "";
		}
	}

	public String getReseau(LocalQueryService localEngine, ManagedEntityValue event) throws CMDBException{
		String result = "-";
		String providerNQL = ":affectedItem = get associated as role a of AffectedBy with :XXXX recursively; get Actor(\"Organization\") referenced as OwnerKey of (get ResponsibleForAssociation where ItemKey in :affectedItem and Responsibility == \"Provider\")";
		ManagedEntityValue provider = et.getObjectWithMev(localEngine, event, providerNQL);
		if (provider != null) {
			result = et.getAttribute(provider, "Name");
		}else{
			String siteGroupNQL = ":affectedItem = get associated as role a of AffectedBy with :XXXX recursively;:affectedItem += get Device associated as role a of Contains with :affectedItem recursively;:affectedItem += get associated as role a of AssignedTo with :affectedItem;:result = get Location(\"Site/Site Group\") associated as role a of Contains, role z of FoundIn with :affectedItem recursively;";
			ManagedEntityValue siteGroup = et.getObjectWithMev(localEngine, event, siteGroupNQL);
			if (siteGroup != null) {
				result = et.getAttribute(siteGroup, "Name");
			}else{
				String indirectProviderNQL = ":affectedItems = get associated as role a of AffectedBy with :XXXX recursively;:affectedItems += get Interface associated as role z of Contains with :affectedItems recursively;:affectedItems += get Device associated as role a of Contains with :affectedItems recursively;:service = get associated as role z of AssignedTo with :affectedItems;:result = get Actor(\"Organization\") referenced as OwnerKey of (get ResponsibleForAssociation where ItemKey in :service and Responsibility == \"Provider\")";
				ManagedEntityValue indirectProvider = et.getObjectWithMev(localEngine, event, indirectProviderNQL);
				if (indirectProvider != null) {
					result = et.getAttribute(indirectProvider, "Name");
				}
			}
		}
		return result;
	}

	public String getReseau(ManagedEntityValue event) throws CMDBException{
		String result = "-";
		String providerNQL = ":affectedItem = get associated as role a of AffectedBy with :XXXX recursively; get Actor(\"Organization\") referenced as OwnerKey of (get ResponsibleForAssociation where ItemKey in :affectedItem and Responsibility == \"Provider\")";
		ManagedEntityValue provider = et.getObjectWithMev(event, providerNQL);
		if (provider != null) {
			result = et.getAttribute(provider, "Name");
		}else{
			String siteGroupNQL = ":affectedItem = get associated as role a of AffectedBy with :XXXX recursively;:affectedItem += get Device associated as role a of Contains with :affectedItem recursively;:affectedItem += get associated as role a of AssignedTo with :affectedItem;:result = get Location(\"Site/Site Group\") associated as role a of Contains, role z of FoundIn with :affectedItem recursively;";
			ManagedEntityValue siteGroup = et.getObjectWithMev(event, siteGroupNQL);
			if (siteGroup != null) {
				result = et.getAttribute(siteGroup, "Name");
			}else{
				String indirectProviderNQL = ":affectedItems = get associated as role a of AffectedBy with :XXXX recursively;:affectedItems += get Interface associated as role z of Contains with :affectedItems recursively;:affectedItems += get Device associated as role a of Contains with :affectedItems recursively;:service = get associated as role z of AssignedTo with :affectedItems;:result = get Actor(\"Organization\") referenced as OwnerKey of (get ResponsibleForAssociation where ItemKey in :service and Responsibility == \"Provider\")";
				ManagedEntityValue indirectProvider = et.getObjectWithMev(event, indirectProviderNQL);
				if (indirectProvider != null) {
					result = et.getAttribute(indirectProvider, "Name");
				}
			}
		}
		return result;
	}

}
