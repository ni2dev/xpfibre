package com.ni2.modules.covage.lifecycle;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Arrays;
import java.util.Map;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Collection;
import java.util.List;
import java.util.ArrayList;
import java.util.TimeZone;
import java.io.FileWriter;

import java.io.IOException;
import java.io.FileNotFoundException;
import com.ni2.cmdb.core.CMDBException;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.ni2.cmdb.core.ItemService;
import com.ni2.cmdb.workflow.WorkflowService;
import com.ni2.cmdb.common.ManagedEntityValue;
import com.ni2.cmdb.common.ManagedEntityKey;
import com.ni2.cmdb.common.ManagedEntityValueIterator;
import com.ni2.cmdb.core.CMDB;
import com.ni2.config.Ni2BaseConfig;


import com.ni2.modules.ni2application.Ni2AppAbstractCMDBService;

import com.ni2.modules.covage.wsftth.WSFTTHUpdateService;



public class CovageFTTHLifecycleServiceImpl extends Ni2AppAbstractCMDBService implements CovageFTTHLifecycleService {
	public static final Log log = LogFactory.getLog(CovageFTTHLifecycleService.class);
	private static final String MESSAGE_PREFIX = "(CovageFTTHLifecycleServiceImpl) ";
	private String logLine;
	private Boolean debug = false;
	private WSFTTHUpdateService wsftth;
	private WorkflowService ws;


	public CovageFTTHLifecycleServiceImpl(CMDB cmdb) throws CMDBException {
		super(cmdb);
		this.wsftth = (WSFTTHUpdateService) cmdb.getCMDBService("WSFTTHUpdateService");
		this.ws = cmdb.getWorkflowService();
	}

	public String getCMDBServiceName() {
		return CMDB_SERVICE_NAME;
	}

	public void documentationOnTroubleTicket(ManagedEntityValue contextMEV, ManagedEntityValue commentMEV) throws CMDBException {
		String msg = MESSAGE_PREFIX + "documentationOnTroubleTicket(" + contextMEV + ")";
		if (log.isDebugEnabled() || debug) log.debug(msg);
		String commentName = (String)commentMEV.getAttributeValue("Name");
		String commentText = (String)commentMEV.getAttributeValue("Comment");
		if (log.isDebugEnabled() || debug) log.debug(MESSAGE_PREFIX + "documentationOnTroubleTicket commentName: " + commentName + " - commentText: "+commentText);
		increaseLastUpdateVersionNumber(contextMEV,false);
		contextMEV.setAttributeValue("MessageContent",commentText);
		contextMEV.setAttributeValue("ActionDescription","SU CASE INFORMATION NOTIFICATION");
		if(commentName.equals("REPONSE RDV")){
			contextMEV.setAttributeValue("MessageType","REPONSE RDV");
			contextMEV.setAttributeValue("SupplierResolutionAction","INTERVENTION AVEC RDV");
			contextMEV.setAttributeValue("SupplierResolutionState","ACTION PLANIFIEE");
			this.is.updateItem(contextMEV);
			this.wsftth.notifyOC_ReponseRDV(contextMEV, commentText);
		}else if(commentName.equals("DEMANDE RDV")){
			contextMEV.setAttributeValue("MessageType","DEMANDE RDV");
			contextMEV.setAttributeValue("SupplierResolutionAction","INTERVENTION AVEC RDV");
			contextMEV.setAttributeValue("SupplierResolutionState","ACTION PREVUE");
			this.is.updateItem(contextMEV);
			this.wsftth.notifyOC_DemandeRDV(contextMEV, commentText);
		}else{
			contextMEV.setAttributeValue("MessageType","INFO");
			this.is.updateItem(contextMEV);
			this.wsftth.notifyOC_Comment(contextMEV, commentText);
		}
	}

	public void changeTroubleTicketState(ManagedEntityValue contextMEV, String newTTS) throws CMDBException{
		String msg = MESSAGE_PREFIX + "changeTroubleTicketState(" + contextMEV + ", " + newTTS + ")";
		if (log.isDebugEnabled() || debug) log.debug(msg);
		if(newTTS == null){
			log.error(MESSAGE_PREFIX + "changeTroubleTicketState - new TTS is null");
			return;
		}
		String currentTTS = (String)contextMEV.getAttributeValue("TroubleTicketState");
		contextMEV.setAttributeValue("TroubleTicketState",newTTS);
		this.is.updateItem(contextMEV);

		if(!currentTTS.equals(newTTS)){
			Collection<ManagedEntityKey> contextMEKList = new ArrayList<>();
			contextMEKList.add(contextMEV.getManagedEntityKey());
			if(currentTTS.equals("QUEUED") && newTTS.equals("CLOSED")){
				this.ws.executeUserAction(contextMEKList, "Cancel", null);
			}else if(currentTTS.equals("QUEUED") && newTTS.equals("OPENACTIVE")){
				contextMEV.setAttributeValue("ActionDescription","CASE SOLVING NOTIFICATION");
				contextMEV.setAttributeValue("SupplierResolutionState","ACTION A DEFINIR");
				this.is.updateItem(contextMEV);
				this.ws.executeUserAction(contextMEKList, "Start", null);
			}else if(currentTTS.equals("OPENACTIVE") && newTTS.equals("OPENACTIVE.TO.BE.CANCELED")){
				contextMEV.setAttributeValue("TroubleTicketToCancel",true);
				this.is.updateItem(contextMEV);
			}else if(currentTTS.equals("OPENACTIVE.TO.BE.CANCELED") && newTTS.equals("CLOSED")){
				contextMEV.setAttributeValue("TroubleTicketToCancel",false);
				contextMEV.setAttributeValue("ActionDescription","CASE CANCELATION AGREEMENT");
				this.is.updateItem(contextMEV);
				this.ws.executeUserAction(contextMEKList, "Cancel", null);
			}else if(currentTTS.equals("OPENACTIVE") && newTTS.equals("CLOSED")){
				contextMEV.setAttributeValue("ActionDescription","CASE SOLVED NOTIFICATION");
				contextMEV.setAttributeValue("SupplierResolutionState","ACTION TERMINEE");
				this.is.updateItem(contextMEV);
				this.ws.executeUserAction(contextMEKList, "Resolve", null);
			}
		}else{
			if (log.isDebugEnabled() || debug) log.debug(MESSAGE_PREFIX + "changeTroubleTicketState - nothing to do as the TroubleTicketStates are equal");
		}
	}

	public void notifyStart(ManagedEntityValue contextMEV) throws CMDBException{
		String msg = MESSAGE_PREFIX + "notifyStart(" + contextMEV + ")";
		if (log.isDebugEnabled() || debug) log.debug(msg);
		increaseLastUpdateVersionNumber(contextMEV,true);
		this.wsftth.notifyOC_StartTroubleTicket(contextMEV);
	}

	public void notifyClose(ManagedEntityValue contextMEV) throws CMDBException{
		String msg = MESSAGE_PREFIX + "notifyClose(" + contextMEV + ")";
		if (log.isDebugEnabled() || debug) log.debug(msg);
		increaseLastUpdateVersionNumber(contextMEV,true);
		this.wsftth.notifyOC_CloseTroubleTicket(contextMEV);
	}

	public void notifyCancel(ManagedEntityValue contextMEV) throws CMDBException{
		String msg = MESSAGE_PREFIX + "notifyCancel(" + contextMEV + ")";
		if (log.isDebugEnabled() || debug) log.debug(msg);
		increaseLastUpdateVersionNumber(contextMEV,false);
		this.wsftth.notifyOC_CancelTroubleTicket(contextMEV);
	}

	public List<String> getPossibleSupplierResolutionStates(ManagedEntityValue contextMEV) throws CMDBException{
		String msg = MESSAGE_PREFIX + "getPossibleSupplierResolutionStates(" + contextMEV + ")";
		if (log.isDebugEnabled() || debug) log.debug(msg);
		List<String> possibleSRS = new ArrayList<>();
		String currentSRS = (String)contextMEV.getAttributeValue("SupplierResolutionState");
		switch(currentSRS){
			case "ACTION A DEFINIR":
				possibleSRS.add("ACTION PREVUE");
				possibleSRS.add("ACTION PLANIFIEE");
				break;
			case "ACTION PREVUE":
				possibleSRS.add("ACTION PLANIFIEE");
				break;
			case "ACTION PLANIFIEE":
				possibleSRS.add("ACTION PREVUE");
				possibleSRS.add("NOUVELLE ACTION A PREVOIR");
				break;
			case "NOUVELLE ACTION A PREVOIR":
				possibleSRS.add("ACTION PREVUE");
				possibleSRS.add("ACTION PLANIFIEE");
				break;
			case "ACTION ANNULEE":
			case "ACTION TERMINEE":
				break;
		}
		return possibleSRS;
	}

	private void increaseLastUpdateVersionNumber(ManagedEntityValue contextMEV, Boolean toUpdate) throws CMDBException {
		Integer lastUpdateVersionNumber = (Integer)contextMEV.getAttributeValue("LastUpdateVersionNumber");
		if(lastUpdateVersionNumber != null){
			lastUpdateVersionNumber = lastUpdateVersionNumber + 1;
			contextMEV.setAttributeValue("LastUpdateVersionNumber",lastUpdateVersionNumber);
		}else{
			String errorMessage = MESSAGE_PREFIX + "cancelTroubleTicket: lastUpdateVersionNumber is not set.";
			log.error(errorMessage);
			throw new CMDBException(errorMessage);
		}
		if(toUpdate)this.is.updateItem(contextMEV);
	}

}
