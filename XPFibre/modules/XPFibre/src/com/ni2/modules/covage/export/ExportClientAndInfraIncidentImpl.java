package com.ni2.modules.covage.export;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.text.DecimalFormat;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.ni2.cmdb.common.ManagedEntityKey;
import com.ni2.cmdb.common.ManagedEntityValue;
import com.ni2.cmdb.core.CMDB;
import com.ni2.cmdb.core.CMDBException;
import com.ni2.cmdb.core.IdentifiableValue;
import com.ni2.cmdb.core.LocalQueryService;
import com.ni2.cmdb.core.NameableValue;
import com.ni2.cmdb.management.EventValue;
import com.ni2.cmdb.system.ObjectInstanceValue;
import com.ni2.cmdb.system.ObjectTypeValue;
import com.ni2.modules.configurationmanagement.ConfigurationManagementService;
import com.ni2.modules.configurationmanagement.ChangeBean;
import com.ni2.modules.ni2foundationimportexport.genericexport.AbstractNQLReport;
import com.ni2.modules.ni2foundationimportexport.genericexport.tools.ManagedEntityReader;
import com.ni2.modules.ni2foundationimportexport.genericexport.tools.export.CSVExportTools;
import com.ni2.modules.ni2foundationimportexport.genericexport.tools.export.ExportTools;
import com.ni2.modules.covage.tools.CovageExportTools;


public class ExportClientAndInfraIncidentImpl extends AbstractNQLReport implements ExportClientAndInfraIncident {
	public static final Log log = LogFactory.getLog(ExportClientAndInfraIncident.class);

	private final ManagedEntityReader et;
	private final CovageExportTools cet;
	private String dataFileName;

	public ExportClientAndInfraIncidentImpl(CMDB cmdb) throws CMDBException {
		super(cmdb);
		this.et = new ManagedEntityReader(cmdb);
		this.cet = (CovageExportTools)getCmdb().getCMDBService("CovageExportTools");
	}

	@Override
	public String getCMDBServiceName() {
		return CMDB_SERVICE_NAME;
	}

	@Override
	protected ExportTools getExportTools(String path, String filename) throws CMDBException {
        return new CSVExportTools(path, filename, ";");
    }

	@Override
	protected String getInterfaceName() {
		return "ExportClientAndInfraIncident";
	}

	@Override
	protected String getProcessType() {
		return "Export - Rapport XPFibre";
	}

	@Override
	public String getProcessDesc() {
		return "Rapport d'incidents client et infra p�riodique";
	}

	@Override
	protected String getTemporaryDataFileName() {
		return "IncidentsClientInfraTemp.csv";
	}

	@Override
	protected String getDataFileName() {
		if(this.dataFileName == null){
			SimpleDateFormat df = new SimpleDateFormat("yyyy_MM_dd_HHmmss");
			String strStartDate = df.format(new Date());
			this.dataFileName = "IncidentsClientInfra_"+strStartDate+".csv";
		}
		return dataFileName;
	}

	@Override
	protected List<String> getHeaders() {
		// XPF-Table-Incident-Report
		List<String> headers = new ArrayList<String>();
		headers.add("#EVENT");
		headers.add("Type");
		headers.add("R�seau");
		headers.add("Client");
		headers.add("Client premium");
		headers.add("Service ID");
		headers.add("Service Premium");
		headers.add("GTR");
		headers.add("Statut");
		headers.add("Cat�gorie");
		headers.add("Urgence");
		headers.add("Impact");
		headers.add("Priorit�");
		headers.add("Groupe");
		headers.add("Date de cr�ation");
		headers.add("MAJ");
		headers.add("Cause");
		headers.add("Temps de r�solution");
		headers.add("Etat d'avancement");
		headers.add("Client final");
		headers.add("Adresse du client final");
		headers.add("POP");
		headers.add("Adresse du POP");
		headers.add("Ref PTO");
		headers.add("Ref PBO");
		headers.add("Site du PBO");
		headers.add("Ref PM");
		headers.add("Adresse du PM");
		headers.add("T�che (ouverte) li�");
		return headers;
	}

	@Override
	protected int getNbKeysInLocalEngine() {
		return 1000;
	};

	@Override
	protected String getContextKeysNQL() {
		// return "get Event(\"Event/Support/Incident/Service Incident\",\"Event/Support/Incident/Infrastructure Incident\") where LastModificationDate > \"2021/05/04 00:00\"";
		return "get Event(\"Event/Support/Incident/Service Incident\",\"Event/Support/Incident/Infrastructure Incident\")";
	}

	@Override
	protected LocalQueryService getLocalEngine(List<ManagedEntityKey> contextKeys) throws CMDBException {
		Map<String, Object> params = new HashMap<String, Object>();
		params.put(":data", contextKeys);

		String nql = ":result = get with specs where managedEntityKey in :data;"+
				":result += get with specs, intermediate associated as role a of AffectedBy with :data;"+
				":result += get with specs, intermediate associated as role a of SubsetOf with :result recursively;"+
				":result += get with specs, intermediate associated as role z of SubsetOf with :result recursively;"+
				":result += get with specs, intermediate associated as role a of AssignedTo with :result;"+
				":result += get with specs, intermediate associated as role a of Contains, role z of FoundIn with :result recursively;"+
				":result += get with specs, intermediate associated as role a of ResponsibleFor with :result;"+
				":result += get with specs, intermediate associated as role z of AllocatedTo with :result;"+
				":result += get with specs, intermediate associated as role a of Localizes with :result;";

		return getItemService().createLocalNQLEngine(getItemService().query(nql, params, new String[] {
				ManagedEntityValue.KEY, IdentifiableValue.IDENTIFIER, NameableValue.NAME, ObjectTypeValue.SUB_CATEGORY_TYPE,
				ObjectInstanceValue.DEFINING_TYPE_KEY, ObjectTypeValue.SUPER_TYPE_KEY, EventValue.STATUS, "Description",
				"LongDescription","CreationDate","ActualStartDate","Urgency","Impact","Priority","Status","NotificationSource",
				"RootCause","Responsibility","ResolutionTime","Debit","City","GTR","Premium","ClosingDate","LastModificationDate","ZipPostalCode","Category","OwnerKey","Street"
				,"ItemKey","EventKey","ActorKey","AddressKey","LocationKey","LocatedKey","ContainerKey","ContainedKey","ResourceKey","ServiceKey","TroubleTicketState","ChildKey","ParentKey"
		}));
	}

	@Override
	protected List<List<String>> getReportLines(LocalQueryService localEngine, ManagedEntityValue context) throws CMDBException {
		List<List<String>> lines = new ArrayList<List<String>>();
		EventValue event = (EventValue)context;

		ManagedEntityValue eventType = et.getObjectWithMev(localEngine, event, "get referenced as DefiningTypeKey of :XXXX");
		ManagedEntityValue provider = et.getObjectWithMev(localEngine, event, "get Actor(\"Organization\") referenced as OwnerKey of (get ResponsibleForAssociation where ItemKey in (get Service associated as role a of AffectedBy with :XXXX) and Responsibility == \"Provider\")");
		ManagedEntityValue service = et.getObjectWithMev(localEngine, event, "get Service associated as role a of AffectedBy with :XXXX");
		ManagedEntityValue customer = et.getObjectWithMev(localEngine, event, "get Actor(\"Organization\") referenced as OwnerKey of (get ResponsibleForAssociation where ItemKey == :XXXX and Responsibility == \"Customer\")");
		ManagedEntityValue handlerGroup = et.getObjectWithMev(localEngine, event, "get Actor(\"Organization\") referenced as OwnerKey of (get ResponsibleForAssociation where ItemKey == :XXXX and Responsibility == \"Administrator\")");
		ManagedEntityValue responsableGroup = et.getObjectWithMev(localEngine, event, "get Actor(\"Organization\") referenced as OwnerKey of (get ResponsibleForAssociation where ItemKey == :XXXX and Responsibility == \"Administrator\")");
		ManagedEntityValue siteClientFinal = et.getObjectWithMev(localEngine, service, "get Location(\"Site/Customer Site\") associated as role z of AllocatedTo with :XXXX ");
		ManagedEntityValue siteClientFinalAddress = et.getObjectWithMev(localEngine, service, "get Address associated as role a of Localizes with (get Location(\"Site/Customer Site\") associated as role z of AllocatedTo with :XXXX )");
		ManagedEntityValue pop = et.getObjectWithMev(localEngine, service, "get Location(\"Site/Provider Site\") associated as role z of FoundIn with (get Device where associated as role a of AssignedTo with :XXXX or associated as role a of Contains with (get Interface associated as role a of AssignedTo with :XXXX ) recursively)");
		ManagedEntityValue popAddress = et.getObjectWithMev(localEngine, pop, "get Address associated as role a of Localizes with :XXXX");
		ManagedEntityValue pto = et.getObjectWithMev(localEngine, service, "get Device(\"Device/Equipment/Network Equipment/PTO\") where associated as role a of AssignedTo with :XXXX or associated as role a of Contains with (get Interface associated as role a of AssignedTo with :XXXX ) recursively");
		ManagedEntityValue pbo = et.getObjectWithMev(localEngine, service, "get Device(\"Device/Equipment/Network Equipment/PBO\") where associated as role a of AssignedTo with :XXXX or associated as role a of Contains with (get Interface associated as role a of AssignedTo with :XXXX ) recursively");
		ManagedEntityValue pboSite = et.getObjectWithMev(localEngine, pbo, "get Location(\"Site/Customer Site\", \"Site/Provider Site\") associated as role a of Contains, role z of FoundIn with :XXXX recursively");
		ManagedEntityValue pm = et.getObjectWithMev(localEngine, service, "get Device(\"Device/Equipment/Network Equipment/PM\") where associated as role a of AssignedTo with :XXXX or associated as role a of Contains with (get Interface associated as role a of AssignedTo with :XXXX ) recursively");
		ManagedEntityValue pmAddress = et.getObjectWithMev(localEngine, pm, "get Address associated as role a of Localizes with (get Location(\"Site/Customer Site\", \"Site/Provider Site\") associated as role a of Contains, role z of FoundIn with :XXXX recursively)");
		ManagedEntityValue tacheLie = et.getObjectWithMev(localEngine, event, "get Event(\"Event/Activity/Task\") associated as role a of SubsetOf with :XXXX where Status != \"Completed\" and Status != \"Canceled\" ;");

		String EVENT_ID = et.getAttribute(event, "Identifier"); // #EVENT
		String EVENT_TYPE = et.getAttribute(eventType, "Name"); // Type
		String EVENT_PROVIDER = cet.getReseau(localEngine, event); // R�seau
		String EVENT_CUSTOMER = et.getAttribute(customer, "Name"); // Client
		String EVENT_CUSTOMER_PREMIUM = et.getAttributeComboLabel(customer, "Premium"); // Client premium
		String EVENT_SERVICE = et.getAttribute(service, "Name"); // Service ID
		String EVENT_SERVICE_PREMIUM = et.getAttributeComboLabel(service, "Premium"); // Service Premium
		String EVENT_SERVICE_GTR = et.getAttribute(service, "GTR"); // GTR
		String EVENT_STATUS = et.getWorkflowStatusLabel(event, "Status"); // Statut
		String EVENT_CATEGORY = et.getAttributeComboLabel(event, "Category"); // Cat�gorie
		String EVENT_URGENCY = et.getAttributeComboLabel(event, "Urgency"); // Urgence
		String EVENT_IMPACT = et.getAttributeComboLabel(event, "Impact"); // Impact
		String EVENT_PRIORITY = et.getAttributeComboLabel(event, "Priority"); // Priorit�
		String EVENT_HANDLER_GROUP = et.getAttribute(handlerGroup, "Name"); // Groupe
		String RFS_DATE_CREATION = et.getDateAttribute(event, "CreationDate","yyyy/MM/dd HH:mm:ss"); // Date de cr�ation
		String EVENT_CLOSING_DATE = et.getDateAttribute(event, "LastModificationDate","yyyy/MM/dd HH:mm:ss"); // MAJ
		String EVENT_ROOT_CAUSE = et.getAttributeComboLabel(event, "RootCause"); // Cause
		String EVENT_RESOLUTION_TIME = cet.getEventFormattedTime(context,"ResolutionTime"); // Temps de r�solution
		String EVENT_TROUBLE_TICKET_STATE = et.getAttribute(event,"TroubleTicketState"); // Etat d'avancement
		String EVENT_CLIENTFINAL = et.getAttribute(siteClientFinal, "Name"); // Client final
		String EVENT_CLIENTFINAL_STREET = et.getAttribute(siteClientFinalAddress, "Street"); // Adresse du client final
		String EVENT_POP = et.getAttribute(pop, "Name"); // POP
		String EVENT_POP_ADDRESS = et.getAttribute(popAddress, "Name"); // Adresse POP
		String EVENT_REF_PTO = et.getAttribute(pto, "Name"); // Ref PTO
		String EVENT_REF_PBO = et.getAttribute(pbo, "Name"); // Ref PBO
		String EVENT_SITE_PBO = et.getAttribute(pboSite, "Name"); // Site PBO
		String EVENT_REF_PM = et.getAttribute(pm, "Name"); // Ref PM
		String EVENT_SITE_PM = et.getAttribute(pmAddress, "Name"); // Address du PM
		String EVENT_TACHE_LIE = et.getAttribute(tacheLie, "Name"); // T�che (ouverte) li�

		List<String> elements = new ArrayList<>();
		elements.add(EVENT_ID); // #EVENT
		elements.add(EVENT_TYPE); // Type
		elements.add(EVENT_PROVIDER); // R�seau
		elements.add(EVENT_CUSTOMER); // Client
		elements.add(EVENT_CUSTOMER_PREMIUM); // Client premium
		elements.add(EVENT_SERVICE); // Service ID
		elements.add(EVENT_SERVICE_PREMIUM); // Service Premium
		elements.add(EVENT_SERVICE_GTR); // GTR
		elements.add(EVENT_STATUS); // Statut
		elements.add(EVENT_CATEGORY); // Cat�gorie
		elements.add(EVENT_URGENCY); // Urgence
		elements.add(EVENT_IMPACT); // Impact
		elements.add(EVENT_PRIORITY); // Priorit�
		elements.add(EVENT_HANDLER_GROUP); // Groupe
		elements.add(RFS_DATE_CREATION); // Date de cr�ation
		elements.add(EVENT_CLOSING_DATE); // MAJ
		elements.add(EVENT_ROOT_CAUSE); // Cause
		elements.add(EVENT_RESOLUTION_TIME); // Temps de r�solution
		elements.add(EVENT_TROUBLE_TICKET_STATE); // Etat d'avancement
		elements.add(EVENT_CLIENTFINAL); // Client final
		elements.add(EVENT_CLIENTFINAL_STREET); // Adresse du client final
		elements.add(EVENT_POP); // POP
		elements.add(EVENT_POP_ADDRESS); // Adresse POP
		elements.add(EVENT_REF_PTO); // Ref PTO
		elements.add(EVENT_REF_PBO); // Ref PBO
		elements.add(EVENT_SITE_PBO); // Site PBO
		elements.add(EVENT_REF_PM); // Ref PM
		elements.add(EVENT_SITE_PM); // Address du PM
		elements.add(EVENT_TACHE_LIE); // T�che (ouverte) li�

		lines.add(elements);
		return lines;
	}

}
