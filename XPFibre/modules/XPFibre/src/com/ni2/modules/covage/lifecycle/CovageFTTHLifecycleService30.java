package com.ni2.modules.covage.lifecycle;

import java.io.IOException;

import java.util.Map;
import java.util.List;
import com.ni2.cmdb.common.ManagedEntityValue;
import com.ni2.cmdb.common.ManagedEntityKey;
import com.ni2.cmdb.core.CMDBException;
import com.ni2.cmdb.core.CMDBService;
import java.util.Map;

import java.io.File;
import java.io.BufferedWriter;
import java.io.FileWriter;

import java.io.OutputStreamWriter;

/**
 * This class implements functions to clone Events from Ni2.
 *
 */
public interface CovageFTTHLifecycleService30 extends CMDBService {
	/**
	 * CMDB Service Name
	 */
	public static final String CMDB_SERVICE_NAME = "CovageFTTHLifecycleService30";

	public String getCMDBServiceName() ;

	/**
	* notify the OC for the contextual Item
	* @param contextMEV: MEV of the contextual Item
	* @throws CMDBException
	*/
	public void documentationOnTroubleTicket(ManagedEntityValue contextMEV, ManagedEntityValue commentMEV) throws CMDBException;
	public void changeTroubleTicketState(ManagedEntityValue contextMEV, String newTroubleTicketState) throws CMDBException;
	public void notifyStart(ManagedEntityKey contextMEK) throws CMDBException;
	public void notifyClose(ManagedEntityKey contextMEK) throws CMDBException;
	public void notifyCancel(ManagedEntityKey contextMEK) throws CMDBException;
	public void notifyFreeze(ManagedEntityKey contextMEK) throws CMDBException;
	public void notifyUnFreeze(ManagedEntityKey contextMEK) throws CMDBException;
	public void notifyWaiting(ManagedEntityKey contextMEK) throws CMDBException;
	public void notifyClear(ManagedEntityKey contextMEK) throws CMDBException;
	public void notifyUnClear(ManagedEntityKey contextMEK) throws CMDBException;
	public List<String> getPossibleSupplierResolutionStates(String endTTState) throws CMDBException;

}
