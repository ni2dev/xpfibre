package com.ni2.modules.covage.wsftth;

import java.util.Map;
import javax.xml.namespace.QName;
import javax.xml.soap.MessageFactory;
import javax.xml.soap.SOAPHeader;
import javax.xml.soap.SOAPBody;
import javax.xml.soap.SOAPConstants;
import javax.xml.soap.SOAPElement;
import javax.xml.soap.SOAPEnvelope;
import javax.xml.soap.SOAPException;
import javax.xml.soap.SOAPMessage;
import javax.xml.soap.SOAPPart;
import javax.xml.ws.Dispatch;
import javax.xml.ws.Service;
import javax.xml.ws.soap.SOAPBinding;

import com.ni2.cmdb.module.ResourcesService;

import com.ni2.config.Ni2BaseConfig;


import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import java.io.FileNotFoundException;
import java.io.IOException;
import javax.xml.parsers.ParserConfigurationException;
import org.xml.sax.SAXException;
import com.ni2.cmdb.core.CMDBException;

import com.ni2.modules.soaptool.ServiceDetail;
import com.ni2.modules.soaptool.ParseWsdlService;



public class IFTRServiceDetail extends WSFTTHServiceDetail {

	public IFTRServiceDetail(ResourcesService rs) throws FileNotFoundException, SAXException, IOException, ParserConfigurationException, CMDBException {
		super();
		this.rs = rs;
		String serverRoot = ni2Config.getProperty(Ni2BaseConfig.NI2_JBOSS_SERVER_PATH_PROP);
		String wsdlRepo = this.rs.getProperty("XPFibre","ws.ftth.IFTR.wsdl");
		String wsdlPath = serverRoot + "/conf/XPFibre/resources/WSFTTH/" + wsdlRepo;
		this.setWsdl(wsdlPath);

		this.log = LogFactory.getLog(IFTRServiceDetail.class);
		this.logHead = "(IFTRServiceDetail) ";
		this.debug = true;
	}

	@Override
	public Dispatch buildDispatcher() throws SOAPException, CMDBException {
		System.out.println(logHead + "buildDispatcher()");
		QName serviceQN = new QName(this.getNameSpace(), this.getServiceName());
		QName portQN = new QName(this.getNameSpace(), this.getPortName());

		Service service = Service.create(serviceQN);
		service.addPort(portQN, SOAPBinding.SOAP11HTTP_BINDING, this.getAddressName());

		Dispatch dispatcher = null;
		dispatcher = service.createDispatch(portQN, SOAPMessage.class, Service.Mode.MESSAGE);

		String username = null;
		username = this.rs.getProperty("XPFibre","ws.ftth.IFTR.auth.username");
		String password = null;
		password = this.rs.getProperty("XPFibre","ws.ftth.IFTR.auth.password");
		System.out.println(logHead + "Special Username: " + username + " Password: hiddenpassword");

		dispatcher.getRequestContext().put(Dispatch.USERNAME_PROPERTY, username);
		dispatcher.getRequestContext().put(Dispatch.PASSWORD_PROPERTY, password);

		return dispatcher;
	}
}