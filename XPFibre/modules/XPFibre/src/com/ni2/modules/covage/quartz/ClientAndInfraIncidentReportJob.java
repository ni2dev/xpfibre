package com.ni2.modules.covage.quartz;


import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.quartz.JobDataMap;
import org.quartz.JobExecutionContext;
import java.util.List;
import java.util.*;
import com.ni2.cmdb.common.ManagedEntityValue;
import com.ni2.cmdb.common.ManagedEntityKey;
import com.ni2.cmdb.workflow.WorkflowService;
import com.ni2.cmdb.workflow.Ni2WorkflowAction;

import com.ni2.cmdb.common.ManagedEntityValueIterator;

import com.ni2.cmdb.core.CMDB;
import com.ni2.cmdb.core.ItemService;
import com.ni2.cmdb.core.CMDBConnector;
import com.ni2.cmdb.core.CMDBException;
import com.ni2.cmdb.core.Environment;
import com.ni2.cmdb.core.EnvironmentService;
import com.ni2.cmdb.system.ProcessService;
import com.ni2.cmdb.system.ProcessValue;
import com.ni2.cmdb.common.ManagedEntityValue;
import com.ni2.config.Ni2BaseConfig;
import com.ni2.quartz.exception.Ni2JobException;
import com.ni2.quartz.jobs.AbstractNi2StatefulJob;
import com.ni2.security.util.Ni2SecurityUtil;
import com.ni2.cmdb.system.ObjectInstanceValue;

import com.ni2.modules.configurationmanagement.ConfigurationManagementService;
import com.ni2.modules.configurationmanagement.ChangeBean;
import com.ni2.cmdb.system.ChangeValue;
import com.ni2.cmdb.core.CMDBService;
import com.ni2.cmdb.module.ResourcesService;
import com.ni2.modules.covage.export.ExportClientAndInfraIncident;

import com.ni2.config.Ni2BaseConfig;
import com.ni2.modules.administration.tools.ProcessHelper;
import java.text.SimpleDateFormat;
import java.io.FileOutputStream;

public class ClientAndInfraIncidentReportJob extends AbstractNi2StatefulJob {
	private static final Log log = LogFactory.getLog(ClientAndInfraIncidentReportJob.class);
	private CMDB cmdb;

	public static final String ARG_CONTEXT_NAME = "variable";

	public void executeJob(JobExecutionContext context) throws Ni2JobException {
		log.info("*** ClientAndInfraIncidentReportJob --- STARTING");
		JobDataMap jobMap = context.getJobDetail().getJobDataMap();
		String contextName = jobMap.getString(ARG_CONTEXT_NAME);
		Ni2BaseConfig baseConfig = new Ni2BaseConfig();
		try {
			String tenant = baseConfig.getDefaultInventoryName();
			String username = baseConfig.getSecurityDefaultUsername();
			String password = baseConfig.getSecurityDefaultPassword();
			password = Ni2SecurityUtil.decodePassword(password);

			cmdb = connectToEnvironment(tenant, username, password);
			EnvironmentService service = cmdb.getEnvironmentService();
			Environment activeEnvironment = service.findInventoryByName(tenant);
			cmdb.activateEnvironment(activeEnvironment);

			ExportClientAndInfraIncident ei = (ExportClientAndInfraIncident) cmdb.getCMDBService("ExportClientAndInfraIncident");
			ei.report("");
		}

		catch (Exception e) {
			log.error("----------   JOB "+contextName+": "+e.getMessage());
			try {
				cmdb.getPrincipalTransaction().commit();
			}
			catch (CMDBException e1) {
				log.error(e1, e1);
				throw new Ni2JobException("Cannot execute JOB", e);
			}
			log.error(e, e);
			throw new Ni2JobException("Cannot execute JOB", e);
		}
	}

	public CMDB connectToEnvironment(String environmentName, String userName, String password) throws CMDBException {
		if (environmentName == null) {
			String error = "Environment must be setted";
			log.error(error);
			throw new IllegalArgumentException(error);
		}
		CMDB cmdb = CMDBConnector.getService().login(userName, password);
		EnvironmentService service = cmdb.getEnvironmentService();
		Environment activeEnvironment = service.findAggregationViewByName(environmentName);
		if (activeEnvironment == null) {
			activeEnvironment = service.findInventoryByName(environmentName);
		}
		if (activeEnvironment != null) {
			cmdb.activateEnvironment(activeEnvironment);
			return cmdb;
		}
		else {
			String error = "Environment " + environmentName + " cannot be found";
			log.error(error);
			throw new IllegalArgumentException(error);
		}

	}

}
