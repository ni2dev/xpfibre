package com.ni2.modules.covage.lifecycle;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Arrays;
import java.util.Map;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Collection;
import java.util.List;
import java.util.ArrayList;
import java.util.TimeZone;
import java.io.FileWriter;

import java.io.IOException;
import java.io.FileNotFoundException;
import com.ni2.cmdb.core.CMDBException;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.ni2.cmdb.core.ItemService;
import com.ni2.cmdb.workflow.WorkflowService;
import com.ni2.cmdb.common.ManagedEntityValue;
import com.ni2.cmdb.common.ManagedEntityKey;
import com.ni2.cmdb.common.ManagedEntityValueIterator;
import com.ni2.cmdb.core.CMDB;
import com.ni2.config.Ni2BaseConfig;


import com.ni2.modules.ni2application.Ni2AppAbstractCMDBService;

import com.ni2.modules.covage.wsftth.WSFTTHUpdateService30;



public class CovageFTTHLifecycleService30Impl extends Ni2AppAbstractCMDBService implements CovageFTTHLifecycleService30 {
	public static final Log log = LogFactory.getLog(CovageFTTHLifecycleService30.class);
	private static final String MESSAGE_PREFIX = "(CovageFTTHLifecycleService30Impl) ";
	private String logLine;
	private Boolean debug = false;
	private WSFTTHUpdateService30 wsftth;
	private WorkflowService ws;


	public CovageFTTHLifecycleService30Impl(CMDB cmdb) throws CMDBException {
		super(cmdb);
		this.wsftth = (WSFTTHUpdateService30) cmdb.getCMDBService("WSFTTHUpdateService30");
		this.ws = cmdb.getWorkflowService();
	}

	public String getCMDBServiceName() {
		return CMDB_SERVICE_NAME;
	}

	public void documentationOnTroubleTicket(ManagedEntityValue contextMEV, ManagedEntityValue commentMEV) throws CMDBException {
		String msg = MESSAGE_PREFIX + "documentationOnTroubleTicket(" + contextMEV + ")";
		if (log.isDebugEnabled() || debug) log.debug(msg);
		String commentName = (String)commentMEV.getAttributeValue("Name");
		String commentText = (String)commentMEV.getAttributeValue("Comment");
		if (log.isDebugEnabled() || debug) log.debug(MESSAGE_PREFIX + "documentationOnTroubleTicket commentName: " + commentName + " - commentText: "+commentText);
		increaseLastUpdateVersionNumber(contextMEV,false);
		contextMEV.setAttributeValue("MessageContent",commentText);
		contextMEV.setAttributeValue("ActionDescription_30","SU CASE INFORMATION NOTIFICATION");
		if(commentName.equals("REPONSE RDV")){
			contextMEV.setAttributeValue("MessageType","REPONSE RDV");
			contextMEV.setAttributeValue("SupplierResolutionAction","INTERVENTION AVEC RDV");
			contextMEV.setAttributeValue("SupplierResolutionState","ACTION PLANIFIEE");
			this.is.updateItem(contextMEV);
			this.wsftth.notifyOC_ReponseRDV(contextMEV, commentText);
		}else if(commentName.equals("DEMANDE RDV")){
			contextMEV.setAttributeValue("MessageType","DEMANDE RDV");
			contextMEV.setAttributeValue("SupplierResolutionAction","INTERVENTION AVEC RDV");
			contextMEV.setAttributeValue("SupplierResolutionState","ACTION PREVUE");
			this.is.updateItem(contextMEV);
			this.wsftth.notifyOC_DemandeRDV(contextMEV, commentText);
		}else{
			contextMEV.setAttributeValue("MessageType","INFO");
			this.is.updateItem(contextMEV);
			this.wsftth.notifyOC_Comment(contextMEV, commentText);
		}
	}

	public void changeTroubleTicketState(ManagedEntityValue contextMEV, String newTTS) throws CMDBException{
		String msg = MESSAGE_PREFIX + "changeTroubleTicketState(" + contextMEV + ", " + newTTS + ")";
		if (log.isDebugEnabled() || debug) log.error(msg);
		if(newTTS == null){
			log.error(MESSAGE_PREFIX + "changeTroubleTicketState - new TTS is null");
			return;
		}
		String currentTTS = (String)contextMEV.getAttributeValue("TroubleTicketState");
		contextMEV.setAttributeValue("TroubleTicketState",newTTS);
		this.is.updateItem(contextMEV);

		if(!currentTTS.equals(newTTS)){
			Collection<ManagedEntityKey> contextMEKList = new ArrayList<>();
			contextMEKList.add(contextMEV.getManagedEntityKey());
			if(currentTTS.equals("QUEUED") && newTTS.equals("CLOSED")){
				this.ws.executeUserAction(contextMEKList, "Cancel", null);
			}else if(currentTTS.equals("QUEUED") && newTTS.equals("OPENACTIVE")){
				contextMEV.setAttributeValue("ActionDescription_30","CASE SOLVING NOTIFICATION");
				this.is.updateItem(contextMEV);
				this.ws.executeUserAction(contextMEKList, "Start", null);
				notifyStart(contextMEV.getManagedEntityKey());
			}else if(currentTTS.equals("OPENACTIVE") && newTTS.equals("OPENACTIVE.TO.BE.CANCELED")){
				contextMEV.setAttributeValue("ActionDescription_30","CASE CANCELATION REQUEST");
				contextMEV.setAttributeValue("TroubleTicketToCancel",true);
				this.is.updateItem(contextMEV);
			}else if(currentTTS.equals("OPENACTIVE.TO.BE.CANCELED") && newTTS.equals("CLOSED")){
				contextMEV.setAttributeValue("TroubleTicketToCancel",false);
				contextMEV.setAttributeValue("ActionDescription_30","CASE CANCELATION AGREEMENT");
				this.is.updateItem(contextMEV);
				this.ws.executeUserAction(contextMEKList, "Cancel", null);
				notifyCancel(contextMEV.getManagedEntityKey());
			}else if(currentTTS.equals("OPENACTIVE") && newTTS.equals("PENDING.REVIEW")){
				contextMEV.setAttributeValue("ActionDescription_30","CASE MEETING FROZEN NOTIFICATION");
				contextMEV.setAttributeValue("TroubleTicketPendingReview",true);
				this.is.updateItem(contextMEV);
				this.ws.executeUserAction(contextMEKList, "Await Input", null);
				notifyFreeze(contextMEV.getManagedEntityKey());
			}else if(currentTTS.equals("PENDING.REVIEW") && newTTS.equals("OPENACTIVE")){
				contextMEV.setAttributeValue("ActionDescription_30","CASE MEETING UNFROZEN NOTIFICATION");
				contextMEV.setAttributeValue("TroubleTicketPendingReview",false);
				contextMEV.setAttributeValue("SupplierResolutionState","ACTION PLANIFIEE");
				this.is.updateItem(contextMEV);
				this.ws.executeUserAction(contextMEKList, "Reactivate", null);
				notifyUnFreeze(contextMEV.getManagedEntityKey());
			}else if(currentTTS.equals("OPENACTIVE") && newTTS.equals("WAITING")){
				contextMEV.setAttributeValue("ActionDescription_30","CASE FROZEN NOTIFICATION");
				this.is.updateItem(contextMEV);
				this.ws.executeUserAction(contextMEKList, "Await Input", null);
				notifyWaiting(contextMEV.getManagedEntityKey());
			}else if(currentTTS.equals("WAITING") && newTTS.equals("OPENACTIVE")){
				contextMEV.setAttributeValue("ActionDescription_30","CASE UNFROZEN NOTIFICATION");
				this.is.updateItem(contextMEV);
				this.ws.executeUserAction(contextMEKList, "Reactivate", null);
			}else if(currentTTS.equals("PENDING.REVIEW") && newTTS.equals("OPENACTIVE.TO.BE.CANCELED")){
				contextMEV.setAttributeValue("ActionDescription_30","CASE CANCELATION REQUEST");
				contextMEV.setAttributeValue("TroubleTicketPendingReview",false);
				contextMEV.setAttributeValue("TroubleTicketToCancel",true);
				contextMEV.setAttributeValue("SupplierResolutionState","ACTION ANNULEE");
				this.is.updateItem(contextMEV);
				this.ws.executeUserAction(contextMEKList, "Reactivate", null);
			}else if(currentTTS.equals("OPENACTIVE") && newTTS.equals("CLEARED")){
				contextMEV.setAttributeValue("ActionDescription_30","CASE CLEARED NOTIFICATION");
				contextMEV.setAttributeValue("InteractionDateComplete",new Date());
				this.is.updateItem(contextMEV);
				this.ws.executeUserAction(contextMEKList, "Resolve", null);
				notifyClear(contextMEV.getManagedEntityKey());
			}else if(currentTTS.equals("CLEARED") && newTTS.equals("CLOSED")){
				contextMEV.setAttributeValue("ActionDescription_30","CASE CLEARED AGREEMENT");
				contextMEV.setAttributeValue("SupplierResolutionState","ACTION TERMINEE");
				this.is.updateItem(contextMEV);
				this.ws.executeUserAction(contextMEKList, "Close", null);
			}else if(currentTTS.equals("CLEARED") && newTTS.equals("UNCLEARED")){
				contextMEV.setAttributeValue("ActionDescription_30","CASE UNCLEARED NOTIFICATION");
				contextMEV.setAttributeValue("TroubleTicketUncleared",true);
				this.is.updateItem(contextMEV);
				this.ws.executeUserAction(contextMEKList, "Reactivate", null);
			}else if(currentTTS.equals("UNCLEARED") && newTTS.equals("OPENACTIVE")){
				contextMEV.setAttributeValue("ActionDescription_30","CASE SOLVING NOTIFICATION");
				contextMEV.setAttributeValue("TroubleTicketUncleared",false);
				this.is.updateItem(contextMEV);
				notifyUnClear(contextMEV.getManagedEntityKey());
			}
		}else{
			if (log.isDebugEnabled() || debug) log.error(MESSAGE_PREFIX + "changeTroubleTicketState - nothing to do as the TroubleTicketStates are equal");
		}
	}

	public void notifyStart(ManagedEntityKey contextMEK) throws CMDBException{
		String msg = MESSAGE_PREFIX + "notifyStart(" + contextMEK + ")";
		if (log.isDebugEnabled() || debug) log.debug(msg);
		ManagedEntityValue contextMEV = this.is.getManagedEntityValue(contextMEK);
		increaseLastUpdateVersionNumber(contextMEV,true);
		this.wsftth.notifyOC_StartTroubleTicket(contextMEV);
	}

	public void notifyClose(ManagedEntityKey contextMEK) throws CMDBException{
		String msg = MESSAGE_PREFIX + "notifyClose(" + contextMEK + ")";
		if (log.isDebugEnabled() || debug) log.debug(msg);
		ManagedEntityValue contextMEV = this.is.getManagedEntityValue(contextMEK);
		increaseLastUpdateVersionNumber(contextMEV,true);
		this.wsftth.notifyOC_CloseTroubleTicket(contextMEV);
	}

	public void notifyCancel(ManagedEntityKey contextMEK) throws CMDBException{
		String msg = MESSAGE_PREFIX + "notifyCancel(" + contextMEK + ")";
		if (log.isDebugEnabled() || debug) log.debug(msg);
		ManagedEntityValue contextMEV = this.is.getManagedEntityValue(contextMEK);
		increaseLastUpdateVersionNumber(contextMEV,true);
		this.wsftth.notifyOC_CancelTroubleTicket(contextMEV);
	}

	public void notifyFreeze(ManagedEntityKey contextMEK) throws CMDBException{
		String msg = MESSAGE_PREFIX + "notifyToAwaitingInput(" + contextMEK + ")";
		if (log.isDebugEnabled() || debug) log.debug(msg);
		ManagedEntityValue contextMEV = this.is.getManagedEntityValue(contextMEK);
		increaseLastUpdateVersionNumber(contextMEV,true);
		this.wsftth.notifyOC_FreezeTroubleTicket(contextMEV);
	}

	public void notifyUnFreeze(ManagedEntityKey contextMEK) throws CMDBException{
		String msg = MESSAGE_PREFIX + "notifyToAwaitingInput(" + contextMEK + ")";
		if (log.isDebugEnabled() || debug) log.debug(msg);
		ManagedEntityValue contextMEV = this.is.getManagedEntityValue(contextMEK);
		increaseLastUpdateVersionNumber(contextMEV,true);
		this.wsftth.notifyOC_UnFreezeTroubleTicket(contextMEV);
	}

	public void notifyWaiting(ManagedEntityKey contextMEK) throws CMDBException{
		String msg = MESSAGE_PREFIX + "notifyToWaiting(" + contextMEK + ")";
		if (log.isDebugEnabled() || debug) log.debug(msg);
		ManagedEntityValue contextMEV = this.is.getManagedEntityValue(contextMEK);
		increaseLastUpdateVersionNumber(contextMEV,true);
		this.wsftth.notifyOC_WaitingTroubleTicket(contextMEV);
	}

	public void notifyClear(ManagedEntityKey contextMEK) throws CMDBException{
		String msg = MESSAGE_PREFIX + "notifyToAwaitingInput(" + contextMEK + ")";
		if (log.isDebugEnabled() || debug) log.debug(msg);
		ManagedEntityValue contextMEV = this.is.getManagedEntityValue(contextMEK);
		increaseLastUpdateVersionNumber(contextMEV,true);
		this.wsftth.notifyOC_ClearTroubleTicket(contextMEV);
	}

	public void notifyUnClear(ManagedEntityKey contextMEK) throws CMDBException{
		String msg = MESSAGE_PREFIX + "notifyToAwaitingInput(" + contextMEK + ")";
		if (log.isDebugEnabled() || debug) log.debug(msg);
		ManagedEntityValue contextMEV = this.is.getManagedEntityValue(contextMEK);
		increaseLastUpdateVersionNumber(contextMEV,true);
		this.wsftth.notifyOC_UnClearTroubleTicket(contextMEV);
	}

	public List<String> getPossibleSupplierResolutionStates(String endTTState) throws CMDBException{
		String msg = MESSAGE_PREFIX + "getPossibleSupplierResolutionStates(" + endTTState + ")";
		if (log.isDebugEnabled() || debug) log.error(msg);
		List<String> possibleSRS = new ArrayList<>();
		switch(endTTState){
			case "QUEUED":
				possibleSRS.add("ACTION A DEFINIR");
				possibleSRS.add("ACTION PLANIFIEE");
				break;
			case "OPENACTIVE":
				possibleSRS.add("ACTION A DEFINIR");
				possibleSRS.add("ACTION PREVUE");
				possibleSRS.add("ACTION PLANIFIEE");
				possibleSRS.add("NOUVELLE ACTION A PREVOIR");
				break;
			case "WAITING":
				possibleSRS.add("ACTION A DEFINIR");
				possibleSRS.add("ACTION PREVUE");
				possibleSRS.add("ACTION PLANIFIEE");
				break;
			case "PENDING.REVIEW":
				possibleSRS.add("ACTION PLANIFIEE");
				break;
			case "OPENACTIVE.TO.BE.CANCELED":
				possibleSRS.add("ACTION ANNULEE");
				possibleSRS.add("ACTION TERMINEE");
				break;
			case "CLEARED":
				possibleSRS.add("ACTION TERMINEE");
				break;
			case "UNCLEARED":
				possibleSRS.add("ACTION PLANIFIEE");
				possibleSRS.add("NOUVELLE ACTION A PREVOIR");
				break;
			case "CLOSED":
				possibleSRS.add("ACTION ANNULEE");
				possibleSRS.add("ACTION TERMINEE");
				break;
		}
		return possibleSRS;
	}

	public List<String> getPossibleEndTTState(String startTTState) throws CMDBException{
		String msg = MESSAGE_PREFIX + "getPossibleEndTTState(" + startTTState + ")";
		if (log.isDebugEnabled() || debug) log.error(msg);
		List<String> possibleSRS = new ArrayList<>();
		switch(startTTState){
			case "QUEUED":
				possibleSRS.add("OPENACTIVE");
				possibleSRS.add("CLOSED");
				break;
			case "OPENACTIVE":
				possibleSRS.add("WAITING");
				possibleSRS.add("PENDING.REVIEW");
				possibleSRS.add("OPENACTIVE.TO.BE.CANCELED");
				possibleSRS.add("CLEARED");
				break;
			case "WAITING":
				possibleSRS.add("OPENACTIVE");
				break;
			case "PENDING.REVIEW":
				possibleSRS.add("OPENACTIVE");
				possibleSRS.add("OPENACTIVE.TO.BE.CANCELED");
				break;
			case "OPENACTIVE.TO.BE.CANCELED":
				possibleSRS.add("CLOSED");
				break;
			case "CLEARED":
				possibleSRS.add("CLOSED");
				possibleSRS.add("UNCLEARED");
				break;
			case "UNCLEARED":
				possibleSRS.add("OPENACTIVE.TO.BE.CANCELED");
				break;
			case "CLOSED":
				break;
		}
		return possibleSRS;
	}

	public List<String> getPossibleStartTTState(String endTTState) throws CMDBException{
		String msg = MESSAGE_PREFIX + "getPossibleTTState(" + endTTState + ")";
		if (log.isDebugEnabled() || debug) log.error(msg);
		List<String> possibleSRS = new ArrayList<>();
		switch(endTTState){
			case "QUEUED":
				break;
			case "OPENACTIVE":
				possibleSRS.add("QUEUED");
				possibleSRS.add("WAITING");
				possibleSRS.add("UNCLEARED");
				possibleSRS.add("PENDING.REVIEW");
				break;
			case "WAITING":
				possibleSRS.add("OPENACTIVE");
				break;
			case "PENDING.REVIEW":
				possibleSRS.add("OPENACTIVE");
				break;
			case "OPENACTIVE.TO.BE.CANCELED":
				possibleSRS.add("PENDING.REVIEW");
				possibleSRS.add("OPENACTIVE");
				break;
			case "CLEARED":
				possibleSRS.add("OPENACTIVE");
				break;
			case "UNCLEARED":
				possibleSRS.add("CLEARED");
				break;
			case "CLOSED":
				possibleSRS.add("CLEARED");
				possibleSRS.add("QUEUED");
				break;
		}
		return possibleSRS;
	}

	private void increaseLastUpdateVersionNumber(ManagedEntityValue contextMEV, Boolean toUpdate) throws CMDBException {
		Integer lastUpdateVersionNumber = (Integer)contextMEV.getAttributeValue("LastUpdateVersionNumber");
		if(lastUpdateVersionNumber != null){
			lastUpdateVersionNumber = lastUpdateVersionNumber + 1;
			contextMEV.setAttributeValue("LastUpdateVersionNumber",lastUpdateVersionNumber);
		}else{
			contextMEV.setAttributeValue("LastUpdateVersionNumber",1);
			String msg = MESSAGE_PREFIX + "increaseLastUpdateVersionNumber was not set (" + contextMEV + ") setting it to 1.";
			if (log.isDebugEnabled() || debug) log.debug(msg);
		}
		if(toUpdate)this.is.updateItem(contextMEV);
	}

}
