package com.ni2.modules.covage.tools;

import com.ni2.cmdb.core.CMDBService;
import com.ni2.cmdb.core.AbstractCMDBService;

import com.ni2.cmdb.core.CMDBException;

public interface WSLogsService extends CMDBService {
	public static final String CMDB_SERVICE_NAME = "WSLogsService";

	public String getCMDBServiceName();

	/**
	* log the WS SOAP call in wsLog DB
	* @throws CMDBException
	*/
	public void wsLogDB(String nomOC, String sirenOC, String operation, String requete, String reponse, Boolean success, String returnLabel, String troubleTicketKey, String message) throws CMDBException;


	/**
	 * log the WS SOAP call as a Ni2 Process
	 * @throws CMDBException
	 */
	public void wsLogProcess(String sdString, String soapMessageStr, String returnSoapMessageStr, String eventId, String type, Boolean success) throws CMDBException;

}

