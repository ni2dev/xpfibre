package com.ni2.modules.covage.auto;

import java.io.IOException;

import java.util.Map;
import java.util.List;
import com.ni2.cmdb.common.ManagedEntityValue;
import com.ni2.cmdb.common.ManagedEntityKey;
import com.ni2.cmdb.core.CMDBException;
import com.ni2.cmdb.core.CMDBService;
import java.util.Map;

import java.io.File;
import java.io.BufferedWriter;
import java.io.FileWriter;

import java.io.OutputStreamWriter;

/**
 * This class implements functions to clone Events from Ni2.
 *
 */
public interface GestionDeRelanceService extends CMDBService {
	/**
	 * CMDB Service Name
	 */
	public static final String CMDB_SERVICE_NAME = "GestionDeRelanceService";

	public String getCMDBServiceName() ;

	public void relancerIncident() throws CMDBException;
	public void relancerTache() throws CMDBException;

}
