package com.ni2.modules.covage.wsftth;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.List;
import java.util.ArrayList;
import java.io.IOException;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.ni2.cmdb.core.CMDBService;
import com.ni2.cmdb.core.CMDB;
import com.ni2.cmdb.core.AbstractCMDBService;
import com.ni2.cmdb.core.CMDBException;
import com.ni2.cmdb.common.ManagedEntityValue;

import com.ni2.modules.ni2application.Ni2AppAbstractCMDBService;

import com.ni2.cmdb.module.ResourcesService;
import com.ni2.config.Ni2BaseConfig;

import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.util.DefaultPrettyPrinter;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.PreparedStatement;
import java.sql.Statement;

import javax.naming.Context;
import javax.sql.DataSource;
import javax.ejb.EJBException;
import com.ni2.kernel.server.ejb.EJBUtil;


/*
-- OIOC_Actions START
DROP TABLE IF EXISTS oioc_actions;

CREATE TABLE oioc_actions (
	id SERIAL PRIMARY KEY not null,
	status varchar(255) DEFAULT 'PENDING',
	status_message text,
	oc_name varchar(255),
	oc_code_siren varchar(255),
	trouble_ticket_key varchar(255),
	trouble_ticket_state varchar(255),
	trouble_ticket_commercial_id varchar(255),
	case_name varchar(255),
	json_content text,
	username varchar(255),
	action_date timestamp not null default CURRENT_TIMESTAMP
);
-- OIOC_Actions DONE
*/


public class OIOCActionsStoreServiceImpl extends Ni2AppAbstractCMDBService implements OIOCActionsStoreService {

	private static final Log log = LogFactory.getLog(OIOCActionsStoreService.class);
	private static final String MESSAGE_PREFIX = "(OIOCActionsStoreServiceImpl) ";
	private Boolean debug = true;

	public OIOCActionsStoreServiceImpl(CMDB cmdb) throws CMDBException {
		super(cmdb);
	}

	public String getCMDBServiceName() {
		return CMDB_SERVICE_NAME;
	}

	public Connection connectToDb(){
		Connection targetConnection = null;
		try {
			DataSource dataSource = (DataSource) EJBUtil.lookup("java:ni2Complement");
			targetConnection = dataSource.getConnection();
		} catch (Exception ex1) {
			String error = "connection error " + ex1.toString();
			log.error("connectToDb" + error, ex1);
		}
		return targetConnection;
	}

	public void storeOIOCAction(String ocName, String ocCodeSiren, String troubleTicketKey, String troubleTicketState, String troubleTicketCommercialId, String caseName, Map<String,String> objectMap, String username) throws CMDBException{
		log.info(MESSAGE_PREFIX + "saveOIOCAction(" + ocName + ", " + ocCodeSiren + ", " + troubleTicketKey + ", " + troubleTicketState + ", " + troubleTicketCommercialId + ", " + caseName + ", " + objectMap + ", " + username + ")");

		Integer resId = 0;
		Connection targetConnection = connectToDb();
		if(targetConnection != null){

			String jsonContent = "ERROR";
			try {
				jsonContent = new ObjectMapper().writerWithDefaultPrettyPrinter().writeValueAsString(objectMap);
			}catch (IOException ex){
				log.error(MESSAGE_PREFIX + "IO Exception while building jsonContent: "+objectMap, ex);
			}
			log.error(MESSAGE_PREFIX + "jsonContent: \n"+jsonContent);

			try {
				String sqlQuery = "INSERT INTO oioc_actions ( oc_name, "
															+ "oc_code_siren, "
															+ "trouble_ticket_key, "
															+ "trouble_ticket_state, "
															+ "trouble_ticket_commercial_id, "
															+ "case_name, "
															+ "json_content, "
															+ "username) "
															+ "VALUES (?, ?, ?, ?, ?, ?, ?, ?);";
				log.info(MESSAGE_PREFIX + "sqlQuery: " + sqlQuery);
				PreparedStatement insertOIOCAction = targetConnection.prepareStatement( sqlQuery, Statement.RETURN_GENERATED_KEYS);

				insertOIOCAction.setString(1, ocName);
				insertOIOCAction.setString(2, ocCodeSiren);
				insertOIOCAction.setString(3, troubleTicketKey);
				insertOIOCAction.setString(4, troubleTicketState);
				insertOIOCAction.setString(5, troubleTicketCommercialId);
				insertOIOCAction.setString(6, caseName);
				insertOIOCAction.setString(7, jsonContent);
				insertOIOCAction.setString(8, username);

				insertOIOCAction.executeUpdate();
				ResultSet resS = insertOIOCAction.getGeneratedKeys();

				if (resS != null && resS.next()) {
					resId = resS.getInt(1);
				}
				if (log.isDebugEnabled() || debug) log.error(MESSAGE_PREFIX + "Log de l'action de l'op�rateur : " + ocName + ", le case : " + caseName + "et pour le ticket : " + troubleTicketKey+ ", avec l'id "+resId);
				insertOIOCAction.close();
				targetConnection.close();
			} catch (SQLException e) {
				//throw new RuntimeException(e);
				log.error(MESSAGE_PREFIX + "Impossible de logguer la requ�te pour l'op�rateur : " + ocName + ", le case : " + caseName + "et pour le ticket : " + troubleTicketKey, e);
			} catch (Exception ex) {
				log.error(MESSAGE_PREFIX + "Erreur de connection:", ex);
			}
		}else{
			log.error(MESSAGE_PREFIX + "Erreur de connection a java:ni2Complement");
		}
	}

	public String getCustomActions(String filterStatus, String filterOCName, String limitStr) throws CMDBException{
		log.info(MESSAGE_PREFIX + "getCustomActions(" + filterStatus + ", " + filterOCName + ", " + limitStr + ")");

		String allPendingActionsStr = "[";
		Integer resId = 0;
		Connection targetConnection = connectToDb();
		if(targetConnection != null){
			try {
				String sqlQuery = "SELECT id, status, action_date, json_content from oioc_actions";
				log.info(MESSAGE_PREFIX + "sqlQuery: " + sqlQuery);
				Boolean hasWhere = false;
				if(filterStatus !=  null && !filterStatus.equals("")){
					hasWhere = true;
					sqlQuery += " WHERE status = ? ";
				}
				if(filterOCName != null && !filterOCName.equals("")){
					if(hasWhere){
						sqlQuery += "AND oc_name = ? ";
					}else{
						sqlQuery += " WHERE oc_name = ? ";
					}
				}
				sqlQuery += ";";
				log.info(MESSAGE_PREFIX + "sqlQuery: " + sqlQuery);
				PreparedStatement statement = targetConnection.prepareStatement( sqlQuery);
				if(filterStatus !=  null && !filterStatus.equals("")){
					hasWhere = true;
					statement.setString(1, filterStatus);
				}
				if(filterOCName != null && !filterOCName.equals("")){
					if(hasWhere){
						statement.setString(2, filterOCName);
					}else{
						statement.setString(1, filterOCName);
					}
				}
				statement.setFetchDirection(ResultSet.FETCH_FORWARD);
				if(limitStr != null && !limitStr.equals("")){
					log.info(MESSAGE_PREFIX + "setting fetch size to: " + Integer.parseInt(limitStr));
					statement.setMaxRows(Integer.parseInt(limitStr));
				}
				ResultSet resS = statement.executeQuery();

				String id, status, action_date, jsonContent = null;

				if (resS != null) {
					int i = 0;
					while (resS.next()) {
						id = resS.getString("id");
						status = resS.getString("status");
						action_date = resS.getString("action_date");
						jsonContent = resS.getString("json_content");
						if (i != 0) allPendingActionsStr += ",\n";
						allPendingActionsStr += "{\n\"id\":\""+id+"\",\n\"status\":\""+status+"\",\n\"date\":\""+action_date+"\",\n\"action\":";
						allPendingActionsStr += jsonContent;
						allPendingActionsStr += "\n}";
						i++;
					}
				}
				statement.close();
				targetConnection.close();
			} catch (SQLException e) {
				//throw new RuntimeException(e);
				log.error(MESSAGE_PREFIX + "Impossible to get custom actions", e);
			} catch (NumberFormatException eNum) {
				log.error(MESSAGE_PREFIX + "Erreur de parsing du int:", eNum);
			} catch (Exception ex) {
				log.error(MESSAGE_PREFIX + "Connection Error:", ex);
			}
		}else{
			log.error(MESSAGE_PREFIX + "Erreur de connection a java:ni2Complement");
		}
		allPendingActionsStr += "]";
		log.error("allPendingActionsStr: "+allPendingActionsStr);
		return allPendingActionsStr;
	}

	public int storeStatusOIOCAction(String actionIdStr, String status, String message) throws CMDBException{
		log.info(MESSAGE_PREFIX + "storeStatusOIOCAction(" + actionIdStr + ", " + status + ", message)");

		int affectedrows = 0;
		Connection targetConnection = connectToDb();
		if(targetConnection != null){
			try {
				String sqlQuerySelect = "SELECT trouble_ticket_key from oioc_actions WHERE id in (";
				String actionIdList[] = actionIdStr.split(",");
				for (int i = 0 ;i<actionIdList.length ; i++ ) {
					if(i!=0) sqlQuerySelect += ", ";
					sqlQuerySelect += "?";
				}
				sqlQuerySelect += ");";
				log.info(MESSAGE_PREFIX + "sqlQuerySelect: " + sqlQuerySelect);
				PreparedStatement statementSelect = targetConnection.prepareStatement(sqlQuerySelect);
				for (int i = 0 ;i<actionIdList.length ; i++ ) {
					statementSelect.setInt(i + 1, Integer.parseInt(actionIdList[i]));
				}

				statementSelect.setFetchDirection(ResultSet.FETCH_FORWARD);
				ResultSet resS = statementSelect.executeQuery();
				List<String> troubleTicketKeyList = new ArrayList<>();

				if (resS != null) {
					while (resS.next()) {
						troubleTicketKeyList.add(resS.getString("trouble_ticket_key"));
					}
				}
				statementSelect.close();


				String sqlQuery = "UPDATE oioc_actions SET status = ?, status_message = ? WHERE id in (";
				for (int i = 0 ;i<actionIdList.length ; i++ ) {
					if(i!=0) sqlQuery += ", ";
					sqlQuery += "?";
				}
				sqlQuery += ");";
				log.info(MESSAGE_PREFIX + "sqlQuery: " + sqlQuery);
				PreparedStatement insertOIOCAction = targetConnection.prepareStatement(sqlQuery);

				insertOIOCAction.setString(1, status);
				insertOIOCAction.setString(2, message);
				for (int i = 0 ;i<actionIdList.length ; i++ ) {
					insertOIOCAction.setInt(3 + i, Integer.parseInt(actionIdList[i]));
				}
				affectedrows = insertOIOCAction.executeUpdate();

				insertOIOCAction.close();

				String systemStatus = "Active";
				if(status.equals("ERROR")){
					systemStatus = "Error";
				}else if(status.equals("IN PROCESS")){
					systemStatus = "In Process";
				}
				updateTicketsSystemStatus(troubleTicketKeyList,systemStatus);
				targetConnection.close();
			} catch (SQLException e) {
				//throw new RuntimeException(e);
				log.error(MESSAGE_PREFIX + "Impossible to update status of action : " + actionIdStr + " with status : " + status, e);
			} catch (NumberFormatException eNum) {
				log.error(MESSAGE_PREFIX + "Int Parsing Error:", eNum);
			} catch (Exception ex) {
				log.error(MESSAGE_PREFIX + "Connection Error", ex);
			}
		}else{
			log.error(MESSAGE_PREFIX + "Erreur de connection a java:ni2Complement");
		}
		return affectedrows;
	}

	public int storeStatusOIOCActionByOC(String oc, String initStatus, String status, String message) throws CMDBException{
		log.info(MESSAGE_PREFIX + "storeStatusOIOCAction(" + oc + ", " + initStatus + ", " + status + ", message)");

		int affectedrows = 0;
		Connection targetConnection = connectToDb();
		if(targetConnection != null){
			try {
				String sqlQuerySelect = "SELECT trouble_ticket_key from oioc_actions WHERE oc_name = ? and status = ?;";
				log.info(MESSAGE_PREFIX + "sqlQuerySelect: " + sqlQuerySelect);
				PreparedStatement statementSelect = targetConnection.prepareStatement(sqlQuerySelect);
				statementSelect.setString(1, oc);
				statementSelect.setString(2, initStatus);
				statementSelect.setFetchDirection(ResultSet.FETCH_FORWARD);
				ResultSet resS = statementSelect.executeQuery();
				List<String> troubleTicketKeyList = new ArrayList<>();

				if (resS != null) {
					while (resS.next()) {
						troubleTicketKeyList.add(resS.getString("trouble_ticket_key"));
					}
				}
				statementSelect.close();


				String sqlQuery = "UPDATE oioc_actions SET status = ?, status_message = ? WHERE oc_name = ? and status = ?;";
				log.info(MESSAGE_PREFIX + "sqlQuery: " + sqlQuery);
				PreparedStatement insertOIOCAction = targetConnection.prepareStatement( sqlQuery);

				insertOIOCAction.setString(1, status);
				insertOIOCAction.setString(2, message);
				insertOIOCAction.setString(3, oc);
				insertOIOCAction.setString(4, initStatus);

				affectedrows = insertOIOCAction.executeUpdate();
				insertOIOCAction.close();

				String systemStatus = "Active";
				if(status.equals("ERROR")){
					systemStatus = "Error";
				}else if(status.equals("IN PROCESS")){
					systemStatus = "In Process";
				}
				updateTicketsSystemStatus(troubleTicketKeyList, systemStatus);
			} catch (SQLException e) {
				//throw new RuntimeException(e);
				log.error(MESSAGE_PREFIX + "Impossible to update status of action : " + oc + " with status : " + status, e);
			} catch (NumberFormatException eNum) {
				log.error(MESSAGE_PREFIX + "Int Parsing Error:", eNum);
			} catch (Exception ex) {
				log.error(MESSAGE_PREFIX + "Connection Error", ex);
			}
		}else{
			log.error(MESSAGE_PREFIX + "Erreur de connection a java:ni2Complement");
		}
		return affectedrows;
	}

	private void updateTicketsSystemStatus(List<String> troubleTicketKeyList, String systemStatus) throws CMDBException{
		log.info(MESSAGE_PREFIX + "updateTicketsSystemStatus(" + troubleTicketKeyList+", " + systemStatus +")");
		Map<String, Object> params = new HashMap<String, Object>();
		List<ManagedEntityValue> troubleTicketMevList = null;
		if(troubleTicketKeyList !=null && !troubleTicketKeyList.isEmpty()){
			params.put(":ttKeyList",troubleTicketKeyList);
			troubleTicketMevList = query("get Event where Identifier in :ttKeyList", params);
			if(troubleTicketMevList != null){
				for(ManagedEntityValue troubleTicket : troubleTicketMevList){
					log.info(MESSAGE_PREFIX + "updateTicketsSystemStatus: " + (String)troubleTicket.getAttributeValue("Name") + " systemStatus: "+(String)troubleTicket.getAttributeValue("SAV30SystemStatus"));
					troubleTicket.setAttributeValue("SAV30SystemStatus", systemStatus);
					this.is.updateItem(troubleTicket);
					log.info(MESSAGE_PREFIX + "updateTicketsSystemStatus: " + (String)troubleTicket.getAttributeValue("Name") + " systemStatus: "+(String)troubleTicket.getAttributeValue("SAV30SystemStatus"));
				}
			}
		}
	}
}
