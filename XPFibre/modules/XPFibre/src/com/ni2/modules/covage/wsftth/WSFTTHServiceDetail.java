package com.ni2.modules.covage.wsftth;

import java.util.Map;
import java.util.List;
import java.util.ArrayList;
import java.io.FileNotFoundException;
import java.io.IOException;
import javax.xml.namespace.QName;
import javax.xml.soap.*;
import javax.xml.ws.Dispatch;
import javax.xml.ws.Service;
import javax.xml.ws.soap.SOAPBinding;

import com.ni2.cmdb.module.ResourcesService;

import com.ni2.config.Ni2BaseConfig;


import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import javax.xml.parsers.ParserConfigurationException;
import org.xml.sax.SAXException;
import com.ni2.cmdb.core.CMDBException;

import com.ni2.modules.soaptool.ServiceDetail;
import com.ni2.modules.soaptool.ParseWsdlService;



public class WSFTTHServiceDetail extends ServiceDetail {

	public ResourcesService rs;
	public Ni2BaseConfig ni2Config = new Ni2BaseConfig();

	public WSFTTHServiceDetail() throws FileNotFoundException, SAXException, IOException, ParserConfigurationException, CMDBException {
		super();
	}

	public WSFTTHServiceDetail(ResourcesService rs) throws FileNotFoundException, SAXException, IOException, ParserConfigurationException, CMDBException {
		super();
		this.rs = rs;
		String serverRoot = ni2Config.getProperty(Ni2BaseConfig.NI2_JBOSS_SERVER_PATH_PROP);
		String wsdlRepo = this.rs.getProperty("XPFibre","ws.ftth.Default.wsdl");
		String wsdlPath = serverRoot + "/conf/XPFibre/resources/WSFTTH/" + wsdlRepo;
		this.setWsdl(wsdlPath);

		this.log = LogFactory.getLog(BOUYServiceDetail.class);
		this.logHead = "(WSFTTHServiceDetail) ";
		this.debug = true;
	}

	@Override
	public void buildSOAPEnvelope(SOAPEnvelope env) throws SOAPException, CMDBException{
		System.out.println(logHead + "buildSOAPEnvelope(env)");
		env.removeNamespaceDeclaration(env.getPrefix());
		env.addNamespaceDeclaration("soap", "http://schemas.xmlsoap.org/soap/envelope/");
		env.setPrefix("soap");
		env.addNamespaceDeclaration("ns1","http://ossj.org/xml/Common/v1-5");
		env.addNamespaceDeclaration("ns2", "http://ossj.org/xml/Common-CBEBi/v1-5");
		env.addNamespaceDeclaration("ns3", "http://ossj.org/xml/TroubleTicket-CBETrouble/v1-2");
		env.addNamespaceDeclaration("ns4", "http://ftthmut.ftth.com/xml/TroubleTicketValue/v1-2");
		env.addNamespaceDeclaration("ns5", "http://ossj.org/xml/TroubleTicket/v1-2");
		env.addNamespaceDeclaration("ns6", "http://sfr.com/xml/TroubleTicketHeader/v1-0");
	}

	@Override
	public Dispatch buildDispatcher() throws SOAPException, CMDBException {
		System.out.println(logHead + "buildDispatcher()");
		QName serviceQN = new QName(this.getNameSpace(), this.getServiceName());
		QName portQN = new QName(this.getNameSpace(), this.getPortName());

		Service service = Service.create(serviceQN);
		service.addPort(portQN, SOAPBinding.SOAP11HTTP_BINDING, this.getAddressName());

		Dispatch dispatcher = null;
		dispatcher = service.createDispatch(portQN, SOAPMessage.class, Service.Mode.MESSAGE);

		String username = null;
		username = this.rs.getProperty("XPFibre","ws.ftth.Default.auth.username");
		String password = null;
		password = this.rs.getProperty("XPFibre","ws.ftth.Default.auth.password");
		System.out.println(logHead + "Special Username: " + username + " Password: hiddenpassword");

		dispatcher.getRequestContext().put(Dispatch.USERNAME_PROPERTY, username);
		dispatcher.getRequestContext().put(Dispatch.PASSWORD_PROPERTY, password);
		return dispatcher;
	}

	@Override
	public void buildSOAPBody(SOAPBody body, String operationName, Map<String,Object> args) throws SOAPException{
		System.out.println(logHead + "buildSOAPBody Overridden(body, " + operationName + ", " + args.toString() + ")");
		SOAPBodyElement operation = (SOAPBodyElement)body.addChildElement(operationName,"ns5");

		SOAPElement troubleTicketValue = operation.addChildElement("troubleTicketValue","ns5");
		SOAPElement ftthmutTroubleTicket = null;
		SOAPElement entryElement = null;

		Object entryValue;
		String[] entryArr;
		List<String> tt_list = new ArrayList<String>();
		tt_list.add("ns1:lastUpdateVersionNumber");
		tt_list.add("ns2:interactionDate");
		tt_list.add("ns2:description");
		tt_list.add("ns3:troubleTicketKey");
		tt_list.add("ns3:troubleTicketState");
		tt_list.add("ns3:serviceRestoredTime");
		tt_list.add("ns3:FTTHMUTTroubleTicket");

		List<String> ftthmutTT_list = new ArrayList<String>();
		ftthmutTT_list.add("ns4:serviceProviderID");
		ftthmutTT_list.add("ns4:supplierID");
		ftthmutTT_list.add("ns4:supplierName");
		ftthmutTT_list.add("ns4:supplierUnit");
		ftthmutTT_list.add("ns4:supplierPhoneNumber");
		ftthmutTT_list.add("ns4:slaId");
		ftthmutTT_list.add("ns4:serviceProviderTroubleTicketKey");
		ftthmutTT_list.add("ns4:messageType");
		ftthmutTT_list.add("ns4:messageContent");
		ftthmutTT_list.add("ns4:supplierPlannedActionDate");
		ftthmutTT_list.add("ns4:supplierResolutionAction");
		ftthmutTT_list.add("ns4:supplierResolutionState");
		ftthmutTT_list.add("ns4:troubleTicketClosureCode");
		ftthmutTT_list.add("ns4:troubleTicketClosureLabel");
		ftthmutTT_list.add("ns4:defectLocalization");
		ftthmutTT_list.add("ns4:defectResponsibility");
		ftthmutTT_list.add("ns4:reprovisionningId");
		ftthmutTT_list.add("ns4:troubleTicketClosureComment");
		ftthmutTT_list.add("ns4:closureDuration");


		for(String tt_entry : tt_list){
			entryValue = args.get(tt_entry);
			entryArr = tt_entry.split(":");
			if(entryValue != null){
				entryElement = troubleTicketValue.addChildElement(entryArr[1],entryArr[0]);
				entryElement.addTextNode((String)entryValue);
			}else if(tt_entry.equals("ns3:FTTHMUTTroubleTicket")){
				ftthmutTroubleTicket = troubleTicketValue.addChildElement(entryArr[1],entryArr[0]);
			}
		}
		if(ftthmutTroubleTicket != null){
			for(String ftthmutTT_entry : ftthmutTT_list){
				entryValue = args.get(ftthmutTT_entry);
				entryArr = ftthmutTT_entry.split(":");
				if(entryValue != null){
					entryElement = ftthmutTroubleTicket.addChildElement(entryArr[1],entryArr[0]);
					entryElement.addTextNode((String)entryValue);
				}
			}
		}
		SOAPElement resyncRequired = operation.addChildElement("resyncRequired","ns5");
		resyncRequired.addTextNode("false");
	}
}