package com.ni2.modules.covage.tools;

import com.ni2.cmdb.core.CMDBService;
import com.ni2.cmdb.core.AbstractCMDBService;
import com.ni2.cmdb.common.ManagedEntityValue;
import com.ni2.cmdb.core.LocalQueryService;

import com.ni2.cmdb.core.CMDBException;

public interface CovageExportTools extends CMDBService {
	public static final String CMDB_SERVICE_NAME = "CovageExportTools";

	public String getCMDBServiceName();

	public String getEventTotalAwaitingInput(ManagedEntityValue event) throws CMDBException;
	public String getEventFormattedTime(ManagedEntityValue event, String attributeName);
	public String getReseau(LocalQueryService localEngine, ManagedEntityValue event) throws CMDBException;
	public String getReseau(ManagedEntityValue event) throws CMDBException;
	public String getEventResolutionDate(ManagedEntityValue event, String datePattern) throws CMDBException;
	public String getEventResolutionTimeDelay(ManagedEntityValue event, String gtr);
}

