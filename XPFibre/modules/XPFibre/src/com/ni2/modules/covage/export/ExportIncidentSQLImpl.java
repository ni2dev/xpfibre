package com.ni2.modules.covage.export;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.text.DecimalFormat;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.ni2.cmdb.common.ManagedEntityKey;
import com.ni2.cmdb.common.ManagedEntityValue;
import com.ni2.cmdb.core.CMDB;
import com.ni2.cmdb.core.CMDBException;
import com.ni2.cmdb.core.IdentifiableValue;
import com.ni2.cmdb.core.LocalQueryService;
import com.ni2.cmdb.core.NameableValue;
import com.ni2.cmdb.management.EventValue;
import com.ni2.cmdb.system.ObjectInstanceValue;
import com.ni2.cmdb.system.ObjectTypeValue;
import com.ni2.modules.configurationmanagement.ConfigurationManagementService;
import com.ni2.modules.configurationmanagement.ChangeBean;
import com.ni2.modules.ni2foundationimportexport.genericexport.AbstractSQLInsertReport;
import com.ni2.modules.ni2foundationimportexport.genericexport.tools.ManagedEntityReader;
import com.ni2.modules.ni2foundationimportexport.genericexport.tools.export.SQLInsertExportTools;
import com.ni2.modules.ni2foundationimportexport.genericexport.tools.export.ExportTools;
import com.ni2.modules.covage.tools.CovageExportTools;

public class ExportIncidentSQLImpl extends AbstractSQLInsertReport implements ExportIncidentSQL {
	public static final Log log = LogFactory.getLog(ExportIncidentSQL.class);

	private final ManagedEntityReader et;
	private final CovageExportTools cet;
	private String dataFileName;

	public ExportIncidentSQLImpl(CMDB cmdb) throws CMDBException {
		super(cmdb);
		this.et = new ManagedEntityReader(cmdb);
		this.cet = (CovageExportTools)getCmdb().getCMDBService("CovageExportTools");
	}

	@Override
	public String getCMDBServiceName() {
		return CMDB_SERVICE_NAME;
	}

	@Override
	protected ExportTools getExportTools(String path, String filename) throws CMDBException {
        return new SQLInsertExportTools(path, filename);
    }

	@Override
	protected String getInterfaceName() {
		return "ExportIncident_SQL";
	}

	@Override
	protected String getProcessType() {
		return "Export SQL - Rapport XPFibre";
	}

	@Override
	public String getProcessDesc() {
		return "Export SQL - Rapport d'incidents par DSP périodique";
	}

	@Override
	protected String getTemporaryDataFileName() {
		return "Incidentstemp.sql";
	}

	@Override
	protected String getDataFileName() {
		if(this.dataFileName == null){
			SimpleDateFormat df = new SimpleDateFormat("yyyy_MM_dd_HHmmss");
			String strStartDate = df.format(new Date());
			this.dataFileName = "Incidents_"+strStartDate+".sql";
		}
		return dataFileName;
	}

	@Override
	protected String getTableName() {
		return "IncidentParDSP";
	}

	@Override
	protected List<String> getHeaders() {
		// XPF-Table-Incident-Par_DSP
		List<String> headers = new ArrayList<String>();
		headers.add("ID_TICKET");
		headers.add("FOURNISSEUR");
		headers.add("ID_NO_SERVICE");
		headers.add("SERVICE");
		headers.add("PRIORITE");
		headers.add("GRAVITE");
		headers.add("ETAT_TICKET");
		headers.add("TYPE_TICKET");
		headers.add("NATURE");
		headers.add("RESUME");
		headers.add("DESCRIPTION");
		headers.add("NOM_USAGER");
		headers.add("NOM_SUF");
		headers.add("ADRESSE_SUF");
		headers.add("CP_SUF");
		headers.add("COMMUNE_SUF");
		headers.add("DEBIT");
		headers.add("GTR");
		headers.add("DATE_CREATION_TICKET");
		headers.add("DATE_RESOLUTION_TICKET");
		headers.add("DATE_FERMETURE_TICKET");
		headers.add("GEL_TICKET");
		headers.add("DUREE_INCIDENT");
		headers.add("DUREE_DEPASSEMENT_GTR");
		headers.add("RAPPORTEUR");
		headers.add("OPERATEUR");
		headers.add("DESCRIPTION_SOLUTION");
		return headers;
	}

	@Override
	protected int getNbKeysInLocalEngine() {
		return 1000;
	};

	@Override
	protected String getContextKeysNQL() {
		// return "get Event(\"Event/Support/Incident\") where LastModificationDate > \"2021/05/04 00:00\"";
		return "get Event(\"Event/Support/Incident\")";
	}

	@Override
	protected LocalQueryService getLocalEngine(List<ManagedEntityKey> contextKeys) throws CMDBException {
		Map<String, Object> params = new HashMap<String, Object>();
		params.put(":data", contextKeys);

		String nql = ":result = get with specs where managedEntityKey in :data;"+
				":service = get with specs, intermediate associated as role a of AffectedBy with :data;"+
				":result += :service;"+
				":result += get with specs, intermediate associated as role a of ResponsibleFor with :result;"+
				":site = get with specs, intermediate Location associated as role z of AllocatedTo with :service;"+
				":adr = get with specs, intermediate Address associated as role a of Localizes with :site;"+
				":result += :site;:result += :adr;";

		return getItemService().createLocalNQLEngine(getItemService().query(nql, params, new String[] {
				ManagedEntityValue.KEY, IdentifiableValue.IDENTIFIER, NameableValue.NAME, ObjectTypeValue.SUB_CATEGORY_TYPE,
				ObjectInstanceValue.DEFINING_TYPE_KEY, ObjectTypeValue.SUPER_TYPE_KEY, EventValue.STATUS, "Description",
				"LongDescription","CreationDate","ActualStartDate","Urgency","Impact","Status","NotificationSource","CreationDate",
				"RootCause","Responsibility","ResolutionTime","Debit","City","GTR","ClosingDate","ZipPostalCode","Category","OwnerKey"
				,"ItemKey","EventKey","ActorKey","AddressKey","LocationKey"
		}));
	}

	@Override
	protected List<List<String>> getReportLines(LocalQueryService localEngine, ManagedEntityValue context) throws CMDBException {
		List<List<String>> lines = new ArrayList<List<String>>();
		EventValue event = (EventValue)context;

		ManagedEntityValue eventType = et.getObjectWithMev(localEngine, event, "get referenced as DefiningTypeKey of :XXXX");
		ManagedEntityValue eventParentType = et.getObjectWithMev(localEngine, eventType, "get referenced as SuperTypeKey of :XXXX");
		ManagedEntityValue provider = et.getObjectWithMev(localEngine, event, "get Actor(\"Organization\") referenced as OwnerKey of (get ResponsibleForAssociation where ItemKey in (get Service associated as role a of AffectedBy with :XXXX) and Responsibility == \"Provider\")");
		ManagedEntityValue service = et.getObjectWithMev(localEngine, event, "get Service associated as role a of AffectedBy with :XXXX");
		ManagedEntityValue serviceType = et.getObjectWithMev(localEngine, event, "get ServiceType referenced as DefiningTypeKey of (get Service associated as role a of AffectedBy with :XXXX)");
		ManagedEntityValue customer = et.getObjectWithMev(localEngine, event, "get Actor(\"Organization\") referenced as OwnerKey of (get ResponsibleForAssociation where ItemKey == :XXXX and Responsibility == \"Customer\")");
		ManagedEntityValue site = et.getObjectWithMev(localEngine, event, "get Location associated as role z of AllocatedTo with (get Service associated as role a of AffectedBy with :XXXX)");
		ManagedEntityValue address = et.getObjectWithMev(localEngine, event, "get Address associated as role a of Localizes with (get Location associated as role z of AllocatedTo with (get Service associated as role a of AffectedBy with :XXXX))");
		ManagedEntityValue requester = et.getObjectWithMev(localEngine, event, "get Actor(\"Human Resource\") referenced as OwnerKey of (get ResponsibleForAssociation where ItemKey == :XXXX and Responsibility == \"Requester\")");
		ManagedEntityValue handler = et.getObjectWithMev(localEngine, event, "get Actor(\"Human Resource\") referenced as OwnerKey of (get ResponsibleForAssociation where ItemKey == :XXXX and Responsibility == \"Handler\")");

		String EVENT_ID = et.getAttribute(event, "Identifier");
		String EVENT_PROVIDER = et.getAttribute(provider, "Name");
		String EVENT_SERVICE = et.getAttribute(service, "Name");
		String EVENT_SERVICE_TYPE = et.getAttribute(serviceType, "Name");
		String EVENT_URGENCY = et.getAttributeComboLabel(event, "Urgency");
		String EVENT_IMPACT = et.getAttributeComboLabel(event, "Impact");
		String EVENT_STATUS = et.getWorkflowStatusLabel(event, "Status");
		String EVENT_NOTIF_SOURCE = et.getAttributeComboLabel(event, "NotificationSource");
		String EVENT_CATEGORY = et.getAttributeComboLabel(event, "Category");
		String RFS_DESCRIPTION = et.getAttribute(event, "Description");
		// String RFS_DESCRIPTION_DETAILLEE = et.getAttribute(event, "LongDescription").replaceAll("[\000-\037]"," ");
		String RFS_DESCRIPTION_DETAILLEE = et.getAttribute(event, "LongDescription");
		String EVENT_CUSTOMER = et.getAttribute(customer, "Name");
		String EVENT_SITE = et.getAttribute(site, "Name");
		String EVENT_ADDRESS_NAME = et.getAttribute(address, "Name");
		String EVENT_ADDRESS_PC = et.getAttribute(address, "ZipPostalCode");
		String EVENT_ADDRESS_CITY = et.getAttribute(address, "City");
		String EVENT_SERVICE_DEBIT = et.getAttribute(service, "Debit");
		String EVENT_SERVICE_GTR = et.getAttribute(service, "GTR");
		String RFS_DATE_CREATION = et.getDateAttribute(event, "CreationDate","yyyy/MM/dd HH:mm:ss");
		String EVENT_RESOLUTION_DATE = cet.getEventResolutionDate(event,"yyyy/MM/dd HH:mm:ss");
		String EVENT_CLOSING_DATE = et.getDateAttribute(event, "ClosingDate","yyyy/MM/dd HH:mm:ss");
		String EVENT_TOTAL_AWAITING_INPUT = cet.getEventTotalAwaitingInput(context);
		String EVENT_RESOLUTION_TIME = cet.getEventFormattedTime(context,"ResolutionTime");
		String EVENT_RESOLUTION_TIME_DELAY = cet.getEventResolutionTimeDelay(context,EVENT_SERVICE_GTR);
		String EVENT_REQUESTER = et.getAttribute(requester, "Name");
		String EVENT_HANDLER = et.getAttribute(handler, "Name");
		String EVENT_ROOT_CAUSE = et.getAttributeComboLabel(event, "RootCause");

		List<String> elements = new ArrayList<>();
		elements.add(EVENT_ID);
		elements.add(EVENT_PROVIDER);
		elements.add(EVENT_SERVICE);
		elements.add(EVENT_SERVICE_TYPE);
		elements.add(EVENT_URGENCY);
		elements.add(EVENT_IMPACT);
		elements.add(EVENT_STATUS);
		elements.add(EVENT_NOTIF_SOURCE);
		elements.add(EVENT_CATEGORY);
		elements.add(RFS_DESCRIPTION);
		elements.add(RFS_DESCRIPTION_DETAILLEE);
		elements.add(EVENT_CUSTOMER);
		elements.add(EVENT_SITE);
		elements.add(EVENT_ADDRESS_NAME);
		elements.add(EVENT_ADDRESS_PC);
		elements.add(EVENT_ADDRESS_CITY);
		elements.add(EVENT_SERVICE_DEBIT);
		elements.add(EVENT_SERVICE_GTR);
		elements.add(RFS_DATE_CREATION);
		elements.add(EVENT_RESOLUTION_DATE);
		elements.add(EVENT_CLOSING_DATE);
		elements.add(EVENT_TOTAL_AWAITING_INPUT);
		elements.add(EVENT_RESOLUTION_TIME);
		elements.add(EVENT_RESOLUTION_TIME_DELAY);
		elements.add(EVENT_REQUESTER);
		elements.add(EVENT_HANDLER);
		elements.add(EVENT_ROOT_CAUSE);

		lines.add(elements);
		return lines;
	}
}
