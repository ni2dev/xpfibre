package com.ni2.modules.covage.lifecycle;

import java.io.IOException;

import java.util.Map;
import java.util.List;
import com.ni2.cmdb.common.ManagedEntityValue;
import com.ni2.cmdb.common.ManagedEntityKey;
import com.ni2.cmdb.core.CMDBException;
import com.ni2.cmdb.core.CMDBService;
import java.util.Map;

import java.io.File;
import java.io.BufferedWriter;
import java.io.FileWriter;

import java.io.OutputStreamWriter;

/**
 * This class implements functions to clone Events from Ni2.
 *
 */
public interface CovageFTTHLifecycleService extends CMDBService {
	/**
	 * CMDB Service Name
	 */
	public static final String CMDB_SERVICE_NAME = "CovageFTTHLifecycleService";

	public String getCMDBServiceName() ;

	/**
	* notify the OC for the contextual Item
	* @param contextMEV: MEV of the contextual Item
	* @throws CMDBException
	*/
	public void documentationOnTroubleTicket(ManagedEntityValue contextMEV, ManagedEntityValue commentMEV) throws CMDBException;
	public void changeTroubleTicketState(ManagedEntityValue contextMEV, String newTroubleTicketState) throws CMDBException;
	public void notifyStart(ManagedEntityValue contextMEV) throws CMDBException;
	public void notifyClose(ManagedEntityValue contextMEV) throws CMDBException;
	public void notifyCancel(ManagedEntityValue contextMEV) throws CMDBException;
	public List<String> getPossibleSupplierResolutionStates(ManagedEntityValue contextMEV) throws CMDBException;

}
