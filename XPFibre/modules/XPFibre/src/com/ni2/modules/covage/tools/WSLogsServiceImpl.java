package com.ni2.modules.covage.tools;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.io.IOException;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.ni2.cmdb.core.CMDBService;
import com.ni2.cmdb.core.CMDB;
import com.ni2.cmdb.core.AbstractCMDBService;
import com.ni2.cmdb.core.CMDBException;


import com.ni2.cmdb.system.ProcessValue;
import com.ni2.modules.administration.tools.ProcessHelper;

import com.ni2.cmdb.module.ResourcesService;
import com.ni2.config.Ni2BaseConfig;


import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.PreparedStatement;
import java.sql.Statement;


public class WSLogsServiceImpl extends AbstractCMDBService implements WSLogsService {

	private static final Log log = LogFactory.getLog(WSLogsServiceImpl.class);
	private static final String MESSAGE_PREFIX = "(WSLogsServiceImpl) ";
	private Boolean debug = true;

	public WSLogsServiceImpl(CMDB cmdb) throws CMDBException {
		super(cmdb);
	}

	public String getCMDBServiceName() {
		return CMDB_SERVICE_NAME;
	}

	public void wsLogDB(String nomOC, String sirenOC, String operation, String requete, String reponse, Boolean success, String returnLabel, String troubleTicketKey, String message) throws CMDBException{
		log.info(MESSAGE_PREFIX + "wsLogDB(" + nomOC + ", " + sirenOC + ", " + operation + ", " + requete + ", " + reponse + ", " + success + ", " + returnLabel + ", " + troubleTicketKey + ", " + message + ")");

		Integer resId = 0;
		ResourcesService rs = (ResourcesService) getCmdb().getCMDBService("ResourcesService");
		String sourceDriver = rs.getProperty("XPFibre","ws.ftth.wsLogs.driverClassName");
		String sourceURL = rs.getProperty("XPFibre","ws.ftth.wsLogs.url");
		String sourceUser = rs.getProperty("XPFibre","ws.ftth.wsLogs.username");
		String sourcePassword = rs.getProperty("XPFibre","ws.ftth.wsLogs.password");
		if (log.isDebugEnabled() || debug) log.error(MESSAGE_PREFIX + "traceRequeteSOAP - sourceDriver:" + sourceDriver + " - sourceURL:" + sourceURL + " - sourceUser:" + sourceUser + ")");


		String returnCode = "0";
		if(!success) returnCode = "1";
		try {
			Class.forName(sourceDriver).newInstance();
			Connection conn = DriverManager.getConnection(sourceURL, sourceUser, sourcePassword);
			String sqlQuery = "INSERT INTO WsSavMut_OI_TO_OC_FTTHlog ( nom_operateur_commercial, "
																		+ "code_siren_oc, "
																		+ "operation, "
																		+ "requete, "
																		+ "reponse, "
																		+ "returnCode, "
																		+ "returnLabel, "
																		+ "troubleTicketKey, "
																		+ "message ) "
																	+ "VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?);";
			log.info(MESSAGE_PREFIX + "sqlQuery: " + sqlQuery);
			PreparedStatement insertWsLog = conn.prepareStatement( sqlQuery, Statement.RETURN_GENERATED_KEYS);

			insertWsLog.setString(1, nomOC);
			insertWsLog.setString(2, sirenOC);
			insertWsLog.setString(3, operation);
			insertWsLog.setString(4, requete);
			insertWsLog.setString(5, reponse);
			insertWsLog.setString(6, returnCode);
			insertWsLog.setString(7, returnLabel);
			insertWsLog.setString(8, troubleTicketKey);
			insertWsLog.setString(9, message);

			insertWsLog.executeUpdate();
			ResultSet resS = insertWsLog.getGeneratedKeys();

			if (resS != null && resS.first()) {
				resId = resS.getInt(1);
			}
			if (log.isDebugEnabled() || debug) log.error(MESSAGE_PREFIX + "Log de la requ�te et r�ponse SOAP pour l'op�ration " + operation+ ", avec l'id "+resId);
			insertWsLog.close();
		} catch (SQLException e) {
			//throw new RuntimeException(e);
			log.error(MESSAGE_PREFIX + "Impossible de logguer la requ�te pour l'op�rateur : " + nomOC + ", l'op�ration SOAP : " + operation + " et le code retour : " + returnCode + " cf exception : " + returnLabel, e);
		} catch (Exception ex) {
			log.error(MESSAGE_PREFIX + "Erreur de connection:", ex);
		}
	}

	public void wsLogProcess(String sdString, String soapMessageStr, String returnSoapMessageStr, String eventId, String type, Boolean success) throws CMDBException{
		log.info(MESSAGE_PREFIX + "wsLogProcess(" + sdString + ", " + soapMessageStr + ", " + returnSoapMessageStr + ", " + eventId + ", " + type + ", " + success.toString() + ")");
		Date startDate = new Date();
		SimpleDateFormat df = new SimpleDateFormat("yyyy_MM_dd_HHmmssSSS");
		String strStartDate = df.format(startDate);
		String logOutputFolder = new Ni2BaseConfig().getNi2ProcessLogPath();
		if (logOutputFolder != null) {
			if (!logOutputFolder.endsWith("/")) {
				logOutputFolder = logOutputFolder + "/";
			}
		}
		String logFileName ="makeSOAPCall"+strStartDate+".log";
		String logFileURL =logOutputFolder+logFileName;
		String processType = "WS FTTH 2.1";
		String processDescription = eventId + " - " + type;
		ProcessValue process = null;
		FileOutputStream logStream = null;
		try{
			process = ProcessHelper.createProcess(getCmdb(),processType,processDescription, logFileName, "");
			logStream = ProcessHelper.getFileOutputStream(logFileURL,"");
			if (log.isDebugEnabled() || debug) log.error(MESSAGE_PREFIX + "wsLogProcess() - writing in: "+logFileURL);
			ProcessHelper.log(logStream,"\n"+sdString);
			ProcessHelper.log(logStream,"========================================\n");
			ProcessHelper.log(logStream,"Message\n");
			ProcessHelper.log(logStream,"\n"+soapMessageStr);
			ProcessHelper.log(logStream,"========================================\n");
			ProcessHelper.log(logStream,"Return Message\n");
			ProcessHelper.log(logStream,"\n"+returnSoapMessageStr);
			if(success){
				ProcessHelper.finishProcess(getCmdb(), process);
			}else{
				ProcessHelper.failProcess(getCmdb(),process);
			}
		}

		catch (Exception e) {
			log.error(MESSAGE_PREFIX + "----------  Error : "+e.getMessage());
			if (process != null) {
				try {
					ProcessHelper.failProcess(getCmdb(),process);
					getCmdb().getPrincipalTransaction().commit();
				}
				catch (CMDBException e1) {
					log.error(e1, e1);
				}
			}
			log.error(e, e);
		}
	}
}
