package com.ni2.modules.covage.export;

import com.ni2.modules.ni2foundationimportexport.genericexport.ExportService;


public interface ExportClientAndInfraIncident extends ExportService {
	/**
	 * CMDB Service Name
	 */
	public static final String CMDB_SERVICE_NAME = "ExportClientAndInfraIncident";
}
