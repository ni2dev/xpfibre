package com.ni2.modules.covage.wsftth;

import java.io.IOException;

import java.util.Map;
import java.util.List;
import com.ni2.cmdb.common.ManagedEntityValue;
import com.ni2.cmdb.common.ManagedEntityKey;
import com.ni2.cmdb.core.CMDBException;
import com.ni2.cmdb.core.CMDBService;
import java.util.Map;

import java.io.File;
import java.io.BufferedWriter;
import java.io.FileWriter;

import java.io.OutputStreamWriter;

/**
 * This class implements functions to clone Events from Ni2.
 *
 */
public interface WSFTTHUpdateService extends CMDBService {
	/**
	 * CMDB Service Name
	 */
	public static final String CMDB_SERVICE_NAME = "WSFTTHUpdateService";

	public String getCMDBServiceName();

	public void notifyOC_Comment(ManagedEntityValue contextMEV, String comment) throws CMDBException;
	public void notifyOC_DemandeRDV(ManagedEntityValue contextMEV, String comment) throws CMDBException;
	public void notifyOC_ReponseRDV(ManagedEntityValue contextMEV, String comment) throws CMDBException;
	public void notifyOC_StartTroubleTicket(ManagedEntityValue contextMEV) throws CMDBException;
	public void notifyOC_CloseTroubleTicket(ManagedEntityValue contextMEV) throws CMDBException;
	public void notifyOC_CancelTroubleTicket(ManagedEntityValue contextMEV) throws CMDBException;
}
