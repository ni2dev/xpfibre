package com.ni2.modules.covage.quartz;


import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.quartz.JobDataMap;
import org.quartz.JobExecutionContext;
import java.util.List;
import java.util.*;
import com.ni2.cmdb.common.ManagedEntityValue;
import com.ni2.cmdb.common.ManagedEntityKey;
import com.ni2.cmdb.workflow.WorkflowService;
import com.ni2.cmdb.workflow.Ni2WorkflowAction;

import com.ni2.cmdb.common.ManagedEntityValueIterator;

import com.ni2.cmdb.core.CMDB;
import com.ni2.cmdb.core.ItemService;
import com.ni2.cmdb.core.CMDBConnector;
import com.ni2.cmdb.core.CMDBException;
import com.ni2.cmdb.core.Environment;
import com.ni2.cmdb.core.EnvironmentService;
import com.ni2.cmdb.system.ProcessService;
import com.ni2.cmdb.system.ProcessValue;
import com.ni2.cmdb.common.ManagedEntityValue;
import com.ni2.config.Ni2BaseConfig;
import com.ni2.quartz.exception.Ni2JobException;
import com.ni2.quartz.jobs.AbstractNi2StatefulJob;
import com.ni2.security.util.Ni2SecurityUtil;
import com.ni2.cmdb.system.ObjectInstanceValue;

import com.ni2.modules.configurationmanagement.ConfigurationManagementService;
import com.ni2.modules.configurationmanagement.ChangeBean;
import com.ni2.cmdb.system.ChangeValue;
import com.ni2.cmdb.core.CMDBService;
import com.ni2.cmdb.module.ResourcesService;

import com.ni2.config.Ni2BaseConfig;
import com.ni2.modules.administration.tools.ProcessHelper;
import java.text.SimpleDateFormat;
import java.io.FileOutputStream;

public class ResolvedIncidentCloseJob extends AbstractNi2StatefulJob {
	private static final Log log = LogFactory.getLog(ResolvedIncidentCloseJob.class);
	private CMDB cmdb;

	public static final String ARG_CONTEXT_NAME = "variable";
    public static final String ARG_MAIL_VTL_CONTEXT = "mailVtlContext";
    public static final String ARG_MAIL_VTL_PAGE = "mailVtlPage";

	public void executeJob(JobExecutionContext context) throws Ni2JobException {
		log.info("*** ResolvedIncidentCloseJob --- STARTING");
		JobDataMap jobMap = context.getJobDetail().getJobDataMap();
		String contextName = jobMap.getString(ARG_CONTEXT_NAME);
        String mailVtlContext = jobMap.getString(ARG_MAIL_VTL_CONTEXT);
        String mailVtlPage = jobMap.getString(ARG_MAIL_VTL_PAGE);
		FileOutputStream logStream = null;
		ProcessValue process = null;
		Ni2BaseConfig baseConfig = new Ni2BaseConfig();

		StringBuilder msg = null;
		ProcessService ps = null;
		try {
			String tenant = baseConfig.getDefaultInventoryName();
			String username = baseConfig.getSecurityDefaultUsername();
			String password = baseConfig.getSecurityDefaultPassword();
			password = Ni2SecurityUtil.decodePassword(password);

			cmdb = connectToEnvironment(tenant, username, password);
			EnvironmentService service = cmdb.getEnvironmentService();
			Environment activeEnvironment = service.findInventoryByName(tenant);
			cmdb.activateEnvironment(activeEnvironment);

			ItemService is = cmdb.getItemService();
			WorkflowService ws = cmdb.getWorkflowService();

			Date startDate = new Date();
			SimpleDateFormat df = new SimpleDateFormat("yyyy_MM_dd_HHmmss");
			String strStartDate = df.format(startDate);
			String logOutputFolder = baseConfig.getNi2ProcessLogPath();
			if (logOutputFolder != null) {
				if (!logOutputFolder.endsWith("/")) {
					logOutputFolder = logOutputFolder + "/";
				}
			}
			String logFileName ="CloseResolvedIncidents"+strStartDate+".log";
			String logFileURL =logOutputFolder+logFileName;
			String processType = "Close_Resolved_Incidents";
			String processDescription = "Close_Resolved_Incidents";
			process = ProcessHelper.createProcess(cmdb,processType,processDescription, logFileName, "");
			logStream = ProcessHelper.getFileOutputStream(logFileURL,"");


			ResourcesService rs = (ResourcesService) cmdb.getCMDBService("ResourcesService");
			String referenceTime_min = rs.getProperty("XPFibre","minute.number.before.close.resolved.incidents");
			ProcessHelper.log(logStream,"Property referenceTime= "+referenceTime_min+" min");

			long referenceTime_ms = Integer.parseInt(referenceTime_min) * 60 * 1000;
			ProcessHelper.log(logStream, "referenceTime= " + convertMiliSecondsToHMmSs(referenceTime_ms));

			Date today = new Date();
			long today_ms = today.getTime();
			long refDAte_ms = today_ms - referenceTime_ms;
			Date refDate = new Date(refDAte_ms);
			df = new SimpleDateFormat("yyyy/MM/dd HH:mm");
			String refDateStr = df.format(refDate);


			String nqlQuery = "get Event(\"Event/Support/Incident\") where Status==\"Resolved\" "
					+ " and LastModificationDate >= \"2020/03/20 00:00\""
					+ " and NotificationSource != \"WS FTTH 3.0\""
					+ " and LastModificationDate <= \"" + refDateStr + "\"";

			Map<String, Object> params = new HashMap<String, Object>();
        	ManagedEntityValueIterator incidentList = is.query(nqlQuery, params);
			log.info("*** ---ResolvedIncidentCloseJob--- incidentList="+incidentList.getRemainingSize());
			ProcessHelper.log(logStream, "Incidents number= "+incidentList.getRemainingSize());

			int ndx = 0;
        	for (Iterator<ManagedEntityValue> it = incidentList.iterator(); it.hasNext();) {
            	ManagedEntityValue incident = it.next();
            	is.updateItem(incident);
            	ndx ++;
				ProcessHelper.log(logStream,"   ");
				ProcessHelper.log(logStream,"["+ndx+"] incident:"+incident.getAttributeValue("Identifier"));

				boolean hasCloseAction = false;
				Map<String, Object> inputs = new HashMap<String, Object>();
				for (Ni2WorkflowAction act : ws.getAvailableActions((ObjectInstanceValue)incident, "UserAction", inputs)) {
					if (act.getName().equals("Close")) {
						hasCloseAction = true;
					}
				}
				if (hasCloseAction) {
					ArrayList<ManagedEntityKey> eventMeks = new ArrayList<ManagedEntityKey>();
					eventMeks.add(incident.getManagedEntityKey());
					String wsAction = "Close";

					ws.executeUserAction(eventMeks, wsAction, "Fermeture automatique du ticket");
				} else {
					ProcessHelper.log(logStream, "---- ---------- ***Error: The Close action is not defined");
				}

				cmdb.getPrincipalTransaction().commit();
				String incidentIdentif = (String) incident.getAttributeValue("Identifier");
    			ManagedEntityValue updatedIncident = is.queryFirst("get Event where Identifier==\""+incidentIdentif+"\"", params);
    			String newStatus = (String)updatedIncident.getAttributeValue("Status");

				if (newStatus.equals("Closed")) {
					ProcessHelper.log(logStream, "---- ---------- Incident closed");
				} else if (hasCloseAction) {
					ProcessHelper.log(logStream, "---- ----------- ***Error: The Incident cannot be closed, Status="+newStatus);
				}
			}

			ProcessHelper.log(logStream,"----------   JOB "+contextName+" Finished - updated "+ndx+" items");
			ProcessHelper.finishProcess(cmdb, process);
		}

		catch (Exception e) {
			log.error("----------   JOB "+contextName+": "+e.getMessage());
			if (process != null) {
				try {
					ProcessHelper.log(logStream,"ResolvedIncidentCloseJob ERROR--- "+contextName+": "+e.getMessage(),e);
					ProcessHelper.failProcess(cmdb,process);
					cmdb.getPrincipalTransaction().commit();
				}
				catch (CMDBException e1) {
					log.error(e1, e1);
					throw new Ni2JobException("Cannot execute JOB", e);
				}
			}
			log.error(e, e);
			throw new Ni2JobException("Cannot execute JOB", e);
		}
	}

	public CMDB connectToEnvironment(String environmentName, String userName, String password) throws CMDBException {
		if (environmentName == null) {
			String error = "Environment must be setted";
			log.error(error);
			throw new IllegalArgumentException(error);
		}
		CMDB cmdb = CMDBConnector.getService().login(userName, password);
		EnvironmentService service = cmdb.getEnvironmentService();
		Environment activeEnvironment = service.findAggregationViewByName(environmentName);
		if (activeEnvironment == null) {
			activeEnvironment = service.findInventoryByName(environmentName);
		}
		if (activeEnvironment != null) {
			cmdb.activateEnvironment(activeEnvironment);
			return cmdb;
		}
		else {
			String error = "Environment " + environmentName + " cannot be found";
			log.error(error);
			throw new IllegalArgumentException(error);
		}

	}

	private static String convertMiliSecondsToHMmSs(long ms) {
		long s = (long) ms / 1000;
		long h = (long) (s / (3600));
		s = s - (h * 3600);
		long m = (long) (s / (60));
		s = s - (m * 60);

		return h+"h "+m+"m "+s+"s";
	}


}
