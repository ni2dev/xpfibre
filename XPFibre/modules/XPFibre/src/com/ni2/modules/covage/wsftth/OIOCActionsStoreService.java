package com.ni2.modules.covage.wsftth;

import com.ni2.cmdb.core.CMDBService;
import com.ni2.cmdb.core.AbstractCMDBService;

import java.util.Map;

import com.ni2.cmdb.core.CMDBException;
import java.sql.Connection;

public interface OIOCActionsStoreService extends CMDBService {
	public static final String CMDB_SERVICE_NAME = "OIOCActionsStoreService";

	public String getCMDBServiceName();

	public Connection connectToDb();

	/**
	* Stores a new OI -> OC Action
	* @throws CMDBException
	*/
	public void storeOIOCAction(String ocName, String ocCodeSiren, String troubleTicketKey, String troubleTicketState, String troubleTicketCommercialId, String caseName, Map<String,String> objectMap, String username) throws CMDBException;
	public int storeStatusOIOCAction(String actionIdStr, String status, String message) throws CMDBException;
	public int storeStatusOIOCActionByOC(String oc, String initStatus, String status, String message) throws CMDBException;
	public String getCustomActions(String filterStatus, String filterOCName, String limitStr) throws CMDBException;



}

