import com.ni2.cmdb.common.ManagedEntityKey;
import com.ni2.cmdb.common.ManagedEntityValue;
import com.ni2.cmdb.core.CMDB;
import com.ni2.cmdb.core.CMDBException;
import com.ni2.cmdb.core.ItemService;
import com.ni2.cmdb.module.ModuleInterface;
import com.ni2.cmdb.system.SystemRightValue;
import com.ni2.config.Ni2BaseConfig;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;



public class XPFibre implements ModuleInterface {

	public void deployCheck(CMDB cmdb) throws Exception {
		System.out.println("(XPFibre) deployCheck : ...");
	}

	public void deployBefore(CMDB cmdb) {
		System.out.println("(XPFibre) deployBefore : ...");
	}

	public void deployAfter(CMDB cmdb) {
		System.out.println("(XPFibre) deployAfter : ...");
		deleteRight(cmdb);
		System.out.println("(XPFibre) DONE deployAfter : ...");
	}

	public void undeployCheck(CMDB cmdb) throws Exception {
		System.out.println("(XPFibre) undeployCheck : ...");
	}

	public void undeployBefore(CMDB cmdb) {
		System.out.println("(XPFibre) undeployBefore : ...");
	}

	public void undeployAfter(CMDB cmdb) {
		System.out.println("(XPFibre) undeployAfter : ...");
	}

	public void updateCheck(CMDB cmdb) throws Exception {
		System.out.println("(XPFibre) updateCheck : ...");
	}

	public void updateBefore(CMDB cmdb) {
		System.out.println("(XPFibre) updateBefore : ...");
	}

	public void updateAfter(CMDB cmdb) {
		System.out.println("(XPFibre) updateAfter : ...");
		deleteRight(cmdb);
		System.out.println("(XPFibre) DONE updateAfter : ...");
	}

    private void deleteRight(CMDB cmdb) {
		try {
			System.out.println("(XPFibre) Deleting Rights : Looking for rights to delete");
			ItemService is = cmdb.getItemService();
			Ni2BaseConfig config = new Ni2BaseConfig();

			List<String> rightQueryNql = new ArrayList<String>();
			if (config.getDefaultLocale().equals("en")) {
				rightQueryNql.add("Module == \"Configuration Management\" and Group == \"Home\" and Name == \"Session\"");
				rightQueryNql.add("Module == \"Asset Management\" and Group == \"Accessory Contextual Menu\"");
				rightQueryNql.add("Module == \"Asset Management\" and Group == \"Accessory Menu\"");
				rightQueryNql.add("Module == \"Asset Management\" and Group == \"Compute Device Contextual Menu\"");
				rightQueryNql.add("Module == \"Asset Management\" and Group == \"Device Contextual Menu\"");
				rightQueryNql.add("Module == \"Asset Management\" and Group == \"Enclosure Contextual Menu\"");
				rightQueryNql.add("Module == \"Asset Management\" and Group == \"Equipment Menu\" and Name contains \"Add\"");
				rightQueryNql.add("Module == \"Asset Management\" and Group == \"Equipment Specs\"");
				rightQueryNql.add("Module == \"Asset Management\" and Group == \"Floor Contextual Menu\"");
				rightQueryNql.add("Module == \"Asset Management\" and Group == \"Network Device Contextual Menu\"");
				// rightQueryNql.add("Module == \"Asset Management\" and Group == \"Network Item Contextual Menu\"");
				rightQueryNql.add("Module == \"Asset Management\" and Group == \"Room Contextual Menu\"");
				rightQueryNql.add("Module == \"Asset Management\" and Group == \"Software Contextual Menu\"");
				rightQueryNql.add("Module == \"Asset Management\" and Group == \"Software Menu\"");
				rightQueryNql.add("Module == \"Asset Management\" and Group == \"Splice Device Contextual Menu\"");
				rightQueryNql.add("Module == \"Asset Management\" and Group == \"Supporting Structure Contextual Menu\"");
				rightQueryNql.add("Module == \"Asset Management\" and Group == \"Type Template Design\"");
				rightQueryNql.add("Module == \"Asset Management\" and Group == \"Zone Contextual Menu\"");
				rightQueryNql.add("Module == \"Asset Management\" and Group == \"Service Provider Contextual Menu\" and Name == \"Delete\"");
				rightQueryNql.add("Module == \"Asset Management\" and Group == \"Organization Contextual Menu\" and Name == \"Delete\"");
				rightQueryNql.add("Module == \"Asset Management\" and Group == \"Customer Contextual Menu\" and Name == \"Delete\"");
				rightQueryNql.add("Module == \"Asset Management\" and Group == \"Maps Functions\"");
				rightQueryNql.add("Module == \"Deployment Management\" and Group == \"Activities Menu\"");
				rightQueryNql.add("Module == \"Deployment Management\" and Group == \"Activities Table\"");
				rightQueryNql.add("Module == \"Deployment Management\" and Group == \"Plan Contextual Menu\"");
				rightQueryNql.add("Module == \"Deployment Management\" and Group == \"Procedure Contextual Menu\"");
				rightQueryNql.add("Module == \"Deployment Management\" and Group == \"Procedure Menu\"");
				rightQueryNql.add("Module == \"Deployment Management\" and Group == \"Project Contextual Menu\"");
				rightQueryNql.add("Module == \"Deployment Management\" and Group == \"Project Menu\"");
				rightQueryNql.add("Module == \"Deployment Management\" and Group == \"Step Contextual Menu\"");
				rightQueryNql.add("Module == \"Deployment Management\" and Group == \"WF Task Contextual Menu\"");
				rightQueryNql.add("Module == \"Facility Resource Design\"");
				rightQueryNql.add("Module == \"IT Resource Design\"");
				rightQueryNql.add("Module == \"Network Resource Design\"");
				rightQueryNql.add("Module == \"Ni2 Application\" and Group == \"Rule Contextual Menu\"");
				rightQueryNql.add("Module == \"Ni2 Application\" and Group == \"Rule Management\"");
				rightQueryNql.add("Module == \"Ni2 Application\" and Group == \"Specification Contextual Menu\"");
				rightQueryNql.add("Module == \"Ni2 Application\" and Group == \"Homepage\"");
				rightQueryNql.add("Module == \"Service Fulfillment\" and Group == \"Service Contextual Menu\" and Name == \"New Customer\"");
				// rightQueryNql.add("Module == \"Service Fulfillment\" and Group == \"Resource Order Menu\"");
				// rightQueryNql.add("Module == \"Service Fulfillment\" and Group == \"Resource Request Contextual Menu\"");
				rightQueryNql.add("Module == \"Service Fulfillment\" and Group == \"Service Order Menu\"");
				rightQueryNql.add("Module == \"Service Fulfillment\" and Group == \"Service Request Contextual Menu\"");
				rightQueryNql.add("Module == \"Service Portfolio\" and Group == \"Service Contextual Menu\" and Name == \"Delete service\"");
				rightQueryNql.add("Module == \"Visit Management\"");
			}else if(config.getDefaultLocale().equals("fr")){
				rightQueryNql.add("Module == \"Configuration Management\" and Group == \"Page d'accueil\" and Name == \"Session\"");
				rightQueryNql.add("Module == \"Asset Management\" and Group == \"Menu contextuel Accessoire\"");
				rightQueryNql.add("Module == \"Asset Management\" and Group == \"Menu accessoire\"");
				rightQueryNql.add("Module == \"Asset Management\" and Group == \"Menu contextuel Appareil de traitement\"");
				rightQueryNql.add("Module == \"Asset Management\" and Group == \"Menu contextuel appareil\"");
				rightQueryNql.add("Module == \"Asset Management\" and Group == \"Menu contextuel enceinte\"");
				rightQueryNql.add("Module == \"Asset Management\" and Group == \"Menu �quipement\" and Name contains \"Ajouter\"");
				rightQueryNql.add("Module == \"Asset Management\" and Group == \"Sp�cifications d'�quipements\"");
				rightQueryNql.add("Module == \"Asset Management\" and Group == \"Menu contextuel �tage\"");
				rightQueryNql.add("Module == \"Asset Management\" and Group == \"Menu contextuel appareil r�seau\"");
				// rightQueryNql.add("Module == \"Asset Management\" and Group == \"Menu contextuel Item r�seau\"");
				rightQueryNql.add("Module == \"Asset Management\" and Group == \"Menu contextuel salle\"");
				rightQueryNql.add("Module == \"Asset Management\" and Group == \"Menu contextuel logiciel\"");
				rightQueryNql.add("Module == \"Asset Management\" and Group == \"Menu logiciel\"");
				rightQueryNql.add("Module == \"Asset Management\" and Group == \"Menu contextuel appareil � �pisser\"");
				rightQueryNql.add("Module == \"Asset Management\" and Group == \"Menu contextuel structure de support\"");
				rightQueryNql.add("Module == \"Asset Management\" and Group == \"Mod�lisation de mod�le de type\"");
				rightQueryNql.add("Module == \"Asset Management\" and Group == \"Menu contextuel zone\"");
				rightQueryNql.add("Module == \"Asset Management\" and Group == \"Menu contextuel Fournisseur de services\" and Name == \"Supprimer\"");
				rightQueryNql.add("Module == \"Asset Management\" and Group == \"Menu contextuel organisation\" and Name == \"Supprimer\"");
				rightQueryNql.add("Module == \"Asset Management\" and Group == \"Menu contextuel Client\" and Name == \"Supprimer\"");
				rightQueryNql.add("Module == \"Asset Management\" and Group == \"Fonctions cartographiques\"");
				rightQueryNql.add("Module == \"Deployment Management\" and Group == \"Menu Activit�s\"");
				rightQueryNql.add("Module == \"Deployment Management\" and Group == \"Table d'activit�s\"");
				rightQueryNql.add("Module == \"Deployment Management\" and Group == \"Menu contextuel Plan\"");
				rightQueryNql.add("Module == \"Deployment Management\" and Group == \"Menu contextuel Proc�dure\"");
				rightQueryNql.add("Module == \"Deployment Management\" and Group == \"Menu Proc�dure\"");
				rightQueryNql.add("Module == \"Deployment Management\" and Group == \"Menu contextuel Projet\"");
				rightQueryNql.add("Module == \"Deployment Management\" and Group == \"Menu Projet\"");
				rightQueryNql.add("Module == \"Deployment Management\" and Group == \"Menu contextuel Etape\"");
				rightQueryNql.add("Module == \"Deployment Management\" and Group == \"Menu contextuel T�che de WF\"");
				rightQueryNql.add("Module == \"Facility Resource Design\"");
				rightQueryNql.add("Module == \"IT Resource Design\"");
				rightQueryNql.add("Module == \"Network Resource Design\"");
				rightQueryNql.add("Module == \"Ni2 Application\" and Group == \"Menu contextuel R�gle\"");
				rightQueryNql.add("Module == \"Ni2 Application\" and Group == \"Gestion de r�gles\"");
				rightQueryNql.add("Module == \"Ni2 Application\" and Group == \"Menu contextuel de sp�cification\"");
				rightQueryNql.add("Module == \"Ni2 Application\" and Group == \"Page principale\"");
				rightQueryNql.add("Module == \"Service Fulfillment\" and Group == \"Menu Client\" and Name == \"Nouveau client\"");
				// rightQueryNql.add("Module == \"Service Fulfillment\" and Group == \"Menu Commande de ressources\"");
				// rightQueryNql.add("Module == \"Service Fulfillment\" and Group == \"Menu contextuel de commande de ressources\"");
				rightQueryNql.add("Module == \"Service Fulfillment\" and Group == \"Menu Commande de service\"");
				rightQueryNql.add("Module == \"Service Fulfillment\" and Group == \"Menu contextuel de demande de service\"");
				rightQueryNql.add("Module == \"Service Portfolio\" and Group == \"Menu contextuel Service\" and Name == \"Supprimer un service\"");
				rightQueryNql.add("Module == \"Visit Management\"");
			}
			if(rightQueryNql.size() > 0){
				Map<String, Object> params = new HashMap<>();
				String nql = "get SystemRightValue where";
				for(int i=0;i < rightQueryNql.size();i++){
					String subNql = rightQueryNql.get(i);
					if(i!=0)nql += "or";
					nql += " ("+subNql+") ";
				}
				System.out.println("(XPFibre) Deleting Rights from NQL: "+nql);
				List<ManagedEntityKey> toDelete = new ArrayList<>();
				for(ManagedEntityValue mev : is.query(nql, params, new String[] { ManagedEntityValue.KEY, SystemRightValue.NAME,SystemRightValue.MODULE_,SystemRightValue.GROUP_ })) {
					toDelete.add(mev.getManagedEntityKey());
					SystemRightValue right = (SystemRightValue)mev;
					System.out.println("(XPFibre) Deleting Rights : Deleting - Module(" + right.getModule() +") - Group(" + right.getGroup() + ") - Right (" + right.getName() + ") !");
				}

				if (!toDelete.isEmpty()) {
					is.deleteItems(toDelete);
					System.out.println("(XPFibre) Deleting Rights : Done deleting "+toDelete.size()+" rights");
				}else{
					System.out.println("(XPFibre) Deleting Rights : No rights to delete");
				}
			}
		} catch (CMDBException e) {
			System.err.println("ERROR: " + e);
			e.printStackTrace();
			throw new IllegalStateException(e);
		}
	}
}
